const axios = require('axios').default;
const React = require('react');
const cookies = require('js-cookie');
const AuthModal = require('../components/AuthModal');
// const SystemEditor = require('../components/SystemEditor');
const SystemSelector = require('../components/SystemSelector');
const NotificationDock = require('../components/NotificationDock');

const NotificationContext = require('../components/NotificationContext');

class IndexPage extends React.Component
{
	constructor(props)
	{
		super(props);

		this.state = {
			authenticating: true,
			checking: true,
			user: null
		};

		this.checkAuth();
	}

	getSelf = () => new Promise(resolve =>
	{
		axios.get('/me').then(({data}) => resolve(data), () => resolve(null));
	})

	checkAuth = () =>
	{
		this.setState({checking: true});

		return this.getSelf().then(
			user => this.setState({
				authenticating: false,
				checking: false,
				user
			})
		);
	}

	onAuth = (method) => (username, password) =>
	{
		this.ndock.notify('warning', 'Аутентификация', 'Выполняется аутентификация...');

		this.setState({authenticating: true});

		axios.post(`/${method}`, {username, password})
			.then(() => this.checkAuth())
			.then(() =>
			{
				if (this.state.user)
					this.ndock.notify('success', 'Аутентификация', 'Аутентификация прошла успешно');
				else
					throw new Error('Ошибка сервера');
			})
			.catch((ex) =>
			{
				this.setState({authenticating: false});
				this.ndock.notify('danger', 'Аутентификация', `Ошибка при аутентификации: ${ex.message}`);
			});
	}

	onExit = () =>
	{
		cookies.remove('session');
		this.checkAuth();
	}

	render()
	{
		return (
			<React.Fragment>
				<NotificationContext.Provider value={(...args) => this.ndock.notify(...args)}>
					<SystemSelector
						isOpen={!this.state.checking && Boolean(this.state.user)}
						onExit={this.onExit}
						username={this.state.user && this.state.user.username}
					/>
				</NotificationContext.Provider>
				<AuthModal
					isOpen={!this.state.checking && !this.state.user}
					locked={this.state.authenticating}
					onSignin={this.onAuth('signin')}
					onSignup={this.onAuth('signup')}
				/>
				<NotificationDock ref={(r) => this.ndock = r} />
			</React.Fragment>
		);
	}
}

module.exports = IndexPage;
