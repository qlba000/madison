require('moment/locale/ru');
require('moment').locale('ru');

require('./styles/index.scss');

const React = require('react');
const ReactDOM = require('react-dom');
const ReactRedux = require('react-redux');
const Redux = require('redux');
const ReactRouterDOM = require('react-router-dom');
const styled = require('styled-components').default;

const IndexPage = require('./index-page/index.jsx');

const store = Redux.createStore((state = 0, action) => state + 1);


const Routes = () => (
	<React.Fragment>
		<ReactRouterDOM.Route exact path="/frontend" component={IndexPage} />
	</React.Fragment>
);

const root = (
	<ReactRedux.Provider store={store}>
		<ReactRouterDOM.BrowserRouter>
			<Routes />
		</ReactRouterDOM.BrowserRouter>
	</ReactRedux.Provider>
);

ReactDOM.render(root, document.getElementById('react-mount-point'));
