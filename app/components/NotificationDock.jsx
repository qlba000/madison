const _ = require('lodash');
const React = require('react');
const {
	Toast,
	ToastHeader,
	ToastBody
} = require('reactstrap');

class NotificationDock extends React.Component
{
	state = {notifications: []};

	notify(type, subject, message)
	{
		const notification = {type, subject, message, in: true};

		this.setState({
			notifications: [...this.state.notifications, notification]
		});

		setTimeout(() =>
		{
			const notifications = _.clone(this.state.notifications);
			const index = _.indexOf(notifications, notification);

			notifications[index] = {...notifications[index], in: false};

			this.setState({notifications});
		}, 5000);
	}

	render()
	{
		return (
			<div
				style={{
					position: 'fixed',
					right: '0.75rem',
					bottom: '0.75rem',
					zIndex: 10000
				}}
			>
				{this.state.notifications.map((notification) => (
					<Toast
						key={notification.key || (notification.key = _.uniqueId())}
						isOpen={notification.in}
						transition={{
							onExited: () => this.setState({
								notifications: _.without(this.state.notifications, notification)
							})
						}}
					>
						<ToastHeader icon={notification.type}>
							{notification.subject || 'Внимание'}
						</ToastHeader>
						<ToastBody>
							{notification.message}
						</ToastBody>
					</Toast>
				))}
			</div>
		);
	}
}

module.exports = NotificationDock;
