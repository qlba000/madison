const _ = require('lodash');
const React = require('react');
const {
	Button,
	Col,
	DropdownItem,
	DropdownMenu,
	DropdownToggle,
	FormGroup,
	Input,
	Label,
	ListGroup,
	ListGroupItem,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Row,
	UncontrolledDropdown,
	UncontrolledTooltip
} = require('reactstrap');
const {Area} = require('recharts');
const PropTypes = require('prop-types');
const cc = require('color-convert');
const {data} = require('../data');
const Tipped = require('../Tipped');
const MFEditor = require('../MFEditor');
const Chart = require('../Chart');
const {tabulateChart} = require('../utils');

const TippedInput = Tipped('bottom')(Input);

function isFloat(string)
{
	return Number.isFinite(Number.parseFloat(string));
}

class LVEditor extends React.Component
{
	constructor(props)
	{
		super(props);

		this.state = this.getInitialState(props.values);

		_.bindAll(this, 'setValue', 'handleSubmit', 'validate');
	}

	componentWillReceiveProps(newProps)
	{
		if (!this.props.isOpen && newProps.isOpen)
			this.setState(this.getInitialState(newProps.values));
	}

	static propTypes = {
		name: PropTypes.string.isRequired,
		handleSubmit: PropTypes.func.isRequired,
		values: PropTypes.any,
		isOpen: PropTypes.bool.isRequired,
		onClose: PropTypes.func.isRequired
	}

	static defaultProps = {
		values: {}
	}

	getInitialState(initialValues)
	{
		const values = initialValues || {};

		return {
			values: {
				xMin: String(isFloat(values.xMin) ? values.xMin : ''),
				xMax: String(isFloat(values.xMax) ? values.xMax : ''),
				terms: values.terms || []
			},
			errors: !initialValues ? {noChanges: true} : {},
			activeTerm: null,
			termName: '',
			isMFEditorOpen: false,
			MFEditorValues: null
		};
	}

	setValue(fieldName)
	{
		return value =>
		{
			this.setState({
				values: {...this.state.values, [fieldName]: value.target ? value.target.value : value}
			});

			_.defer(() => this.validate());
		};
	}

	handleSubmit()
	{
		if (!_.isEmpty(this.state.errors))
			return;

		const result = {...this.state.values};

		result.xMin = Number(result.xMin);
		result.xMax = Number(result.xMax);

		_.each(result.terms, term =>
		{
			delete term.table;
		});

		this.props.handleSubmit(result);
		this.props.onClose();
	}

	validate()
	{
		const errors = {}, {values} = this.state;

		if (!isFloat(values.xMin))
			errors.xMin = 'Значение Xmin должно быть числом';

		if (!isFloat(values.xMax))
			errors.xMax = 'Значение Xmax должно быть числом';

		if (isFloat(values.xMin) &&
			isFloat(values.xMax) &&
			+values.xMin > +values.xMax
		) {
			errors.xMin = 'Значение Xmin не может превышать значение Xmax';
			errors.xMax = 'Значение Xmax не может быть меньше Xmin';
		}

		this.setState({errors});
	}

	setActiveTerm(name)
	{
		this.setState({activeTerm: name, termName: name || ''});
	}

	mangleActiveTerm(action, delta)
	{
		const termIndex = _.findIndex(this.state.values.terms, {name: this.state.activeTerm});
		const state = _.cloneDeep(this.state);
		const terms = state.values.terms;

		switch (action)
		{
		case 'SAVE':
			if (termIndex < 0)
				terms.push({name: this.state.termName, ...delta});
			else
				terms[termIndex] = {...terms[termIndex], ...delta, table: null};
			state.activeTerm = state.termName;
			break;
		case 'REMOVE':
			terms.splice(termIndex, 1);
			state.activeTerm = null;
			state.termName = '';
			break;
		case 'EDITMF':
			state.MFEditorValues = terms[termIndex].mf;
			state.isMFEditorOpen = true;
			break;
		default:
			return;
		}

		this.setState(state);
	}

	isTermNameValid(name)
	{
		return name !== '' && !_.find(this.state.values.terms, {name});
	}

	render()
	{
		return (
			<React.Fragment>
				<Modal
					id="lveditor-modal"
					backdrop="static"
					isOpen={this.props.isOpen}
					// toggle={this.props.onClose}
					centered
					className="wide-modal"
				>
					<ModalHeader>
						Лингвистическая переменная {`"${this.props.name}"`}
					</ModalHeader>
					<ModalBody style={{minHeight: '501px'}}>
						{this.renderBody()}
					</ModalBody>
					<ModalFooter>
						<Button
							color="secondary"
							onClick={this.props.onClose}
						>
							Отмена
						</Button>
						<Button
							color="primary"
							onClick={() => this.handleSubmit()}
							disabled={!_.isEmpty(this.state.errors) || _.isEmpty(this.state.values.terms)}
						>
							ОК
						</Button>
					</ModalFooter>
				</Modal>
				<MFEditor
					isOpen={this.state.isMFEditorOpen}
					onClose={() => this.setState({isMFEditorOpen: false})}
					onSubmit={mf => this.mangleActiveTerm('SAVE', {mf})}
					xMin={Number(this.state.values.xMin)}
					xMax={Number(this.state.values.xMax)}
					values={this.state.MFEditorValues}
				/>
			</React.Fragment>
		);
	}

	renderBody()
	{
		return (
			<React.Fragment>
				{this.renderLVFields()}
				<hr />
				{_.isEmpty(this.state.errors) ? (
					<React.Fragment>
						{this.renderTerms()}
					</React.Fragment>
				) : (
					<div
						style={{
							textAlign: 'center',
							color: '#666',
							fontSize: '32px',
							position: 'absolute',
							top: '260px',
							width: '100%'
						}}
					>
						Укажите область определения переменной
					</div>
				)}
			</React.Fragment>
		);
	}

	renderLVFields()
	{
		const {values, errors} = this.state;

		return (
			<Row>
				<Col md={6}>
					<FormGroup row className="mb-0 mt-2">
						<Label for="xMin" lg={2} md={3} sm={2}><abbr title="Нижняя граница области значений переменной">Xmin</abbr></Label>
						<Col lg={10} md={9} sm={10}>
							<TippedInput
								type="text"
								name="xMin"
								id="xMin"
								error={errors.xMin}
								value={values.xMin}
								onChange={this.setValue('xMin')}
								disabled={!_.isEmpty(values.terms)}
							/>
						</Col>
					</FormGroup>
				</Col>
				<Col md={6}>
					<FormGroup row className="mb-0 mt-2">
						<Label for="xMax" lg={2} md={3} sm={2}><abbr title="Верхняя граница области значений переменной">Xmax</abbr></Label>
						<Col lg={10} md={9} sm={10}>
							<TippedInput
								type="text"
								name="xMax"
								id="xMax"
								error={errors.xMax}
								value={values.xMax}
								onChange={this.setValue('xMax')}
								disabled={!_.isEmpty(values.terms)}
							/>
						</Col>
					</FormGroup>
				</Col>
			</Row>
		);
	}

	renderTerms()
	{
		return (
			<Row>
				<Col xs={5} className="border-right">
					{this.renderTermList()}
				</Col>
				<Col xs={7}>
					{this.renderTermMenu()}
					<hr />
					<Row className="pr-2">
						{this.renderChart()}
					</Row>
				</Col>
			</Row>
		);
	}

	renderTermList()
	{
		const {terms} = this.state.values;

		return (
			<ListGroup>
				{terms.map(({name}, index) => (
					<ListGroupItem
						action
						active={this.state.activeTerm === name}
						onClick={() => this.setActiveTerm(name)}
						style={{
							backgroundColor: this.state.activeTerm === name ? undefined : `#${cc.hwb.hex((index * 30 + 90) % 360, 85, 0)}`
						}}
					>
						{name}
					</ListGroupItem>
				))}
				<ListGroupItem
					action
					active={this.state.activeTerm === null}
					onClick={() => this.setActiveTerm(null)}
				>
					Новый терм
				</ListGroupItem>
			</ListGroup>
		);
	}

	renderTermMenu()
	{
		const {termName, activeTerm} = this.state;

		return (
			<div style={{display: 'flex'}} className="mb-2">
				<Input
					id="termName"
					type="text"
					name="termName"
					placeholder={activeTerm === null ? 'Название нового терма' : 'Новое название терма'}
					onChange={({target}) => this.setState({termName: target.value})}
					value={termName}
					style={{flexGrow: 1, flexShrink: 1, minWidth: 0}}
					disabled={!_.isEmpty(this.state.errors)}
				/>
				{activeTerm === null && this.isTermNameValid(termName) && (
					<Button
						className="ml-2"
						color="primary"
						onClick={() => this.setState({
							isMFEditorOpen: true,
							MFEditorValues: null
						})}
					>
						Создать
					</Button>
				)}
				{activeTerm !== null && (
					<UncontrolledDropdown>
						<DropdownToggle caret color="primary" className="ml-2">
							Действия
						</DropdownToggle>
						<DropdownMenu right>
							<DropdownItem
								id="rename-term"
								// disabled={!this.isTermNameValid(termName)}
								onClick={() =>
									this.isTermNameValid(termName) &&
									this.mangleActiveTerm('SAVE', {name: this.state.termName})
								}
								// className="text-warning"
							>
								Переименовать
							</DropdownItem>
							{(termName === activeTerm) && (
								<UncontrolledTooltip
									target="rename-term"
									placement="left"
									delay={0}
								>
									Измените имя терма в поле ввода
								</UncontrolledTooltip>
							)}
							<DropdownItem
								onClick={() => this.mangleActiveTerm('EDITMF')}
								// className="text-info"
							>
								Редактировать
							</DropdownItem>
							<DropdownItem
								onClick={() => this.mangleActiveTerm('REMOVE')}
								// className="text-danger"
							>
								Удалить
							</DropdownItem>
						</DropdownMenu>
					</UncontrolledDropdown>
				)}
			</div>
		);
	}

	renderChart()
	{
		return (
			<Chart
				xMin={Number(this.state.values.xMin)}
				xMax={Number(this.state.values.xMax)}
			>
				{
					_.map(this.state.values.terms, (term, index) =>
					{
						const color = `#${cc.hwb.hex((index * 30 + 90) % 360, 0, 0)}`;

						if (!term.table)
							term.table = tabulateChart(
								data[term.mf.type].func,
								term.mf,
								Number(this.state.values.xMin),
								Number(this.state.values.xMax),
								_.isEmpty(this.state.errors)
							);

						return (
							<Area
								name={term.name}
								data={term.table}
								dataKey="y"
								stroke={color}
								fill={color}
								fillOpacity={term.name === this.state.activeTerm ? 0.3 : 0.08}
							/>
						);
					})
				}
			</Chart>
		);
	}
}

module.exports = LVEditor;
