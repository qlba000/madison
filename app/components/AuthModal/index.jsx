const React = require('react');
const {
	Button,
	Input,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	UncontrolledTooltip
} = require('reactstrap');
const PropTypes = require('prop-types');

class AuthModal extends React.Component
{
	static propTypes = {
		isOpen: PropTypes.bool.isRequired,
		locked: PropTypes.bool.isRequired,
		onSignin: PropTypes.func.isRequired,
		onSignup: PropTypes.func.isRequired
	}

	componentWillReceiveProps(newProps)
	{
		if (this.props.isOpen && !newProps.isOpen)
			this.setState({
				username: '',
				password: '',
				touched: false
			});
	}

	state = {
		username: '',
		password: '',
		touched: false
	}

	onChange = name => ev => this.setState({...this.state, [name]: ev.target.value})

	withVals = func => () =>
	{
		this.setState({touched: true});

		if (!this.state.username || !this.state.password)
			return;

		return func(this.state.username, this.state.password);
	}

	render()
	{
		const {username, password} = this.state;
		const {locked} = this.props;

		return (
			<Modal
				id="auth-modal"
				backdrop="static"
				isOpen={this.props.isOpen}
				// toggle={this.props.toggle}
				centered
			>
				<ModalHeader>
					Вход/регистрация
				</ModalHeader>
				<ModalBody>
					{this.renderBody()}
				</ModalBody>
				<ModalFooter>
					<Button
						id="register-button"
						color="info"
						onClick={this.withVals(this.props.onSignup)}
						disabled={locked}
					>
						Регистрация
					</Button>
					{(!username || !password) && (
						<UncontrolledTooltip
							target="register-button"
							delay={0}
						>
							Введите имя пользователя и пароль, которые вы
							хотите использовать для входа в систему
						</UncontrolledTooltip>
					)}
					<Button
						color="primary"
						onClick={this.withVals(this.props.onSignin)}
						disabled={locked}
					>
						Вход
					</Button>
				</ModalFooter>
			</Modal>
		);
	}

	renderBody()
	{
		const {username, password, touched} = this.state;

		return (
			<React.Fragment>
				<Input
					id="username"
					name="username"
					type="text"
					placeholder="Имя пользователя"
					className="w-100 mb-2"
					value={username}
					invalid={!username && touched}
					disabled={this.props.locked}
					onChange={this.onChange('username')}
				/>
				<Input
					id="password"
					name="password"
					type="password"
					placeholder="Пароль"
					className="w-100"
					value={password}
					invalid={!password && touched}
					onChange={this.onChange('password')}
					disabled={this.props.locked}
				/>
			</React.Fragment>
		);
	}
}

module.exports = AuthModal;
