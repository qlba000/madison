const React = require('react');
const {Input} = require('reactstrap');
const PropTypes = require('prop-types');

function RegexInput({regex, value, ...props})
{
	const onChange = (event) =>
	{
		if (regex.test(event.target.value))
			props.onChange(event);
	};

	return (
		<Input
			{...props}
			type="text"
			value={value}
			onChange={onChange}
		/>
	);
}

RegexInput.propTypes = {
	regex: PropTypes.object.isRequired,
	value: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired
};

module.exports = RegexInput;
