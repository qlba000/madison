const axios = require('axios').default;
const React = require('react');
const {
	Col,
	Button,
	Input,
	ListGroup,
	ListGroupItem,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Row,
	Spinner,
	UncontrolledTooltip
} = require('reactstrap');
const moment = require('moment');
const ellipsize = require('ellipsize');
const PropTypes = require('prop-types');
const NotificationContext = require('../NotificationContext');
const SystemEditor = require('../SystemEditor');

const DEFAULT_SYSTEM = {
	options: {
		model: {
			value: 'logical'
		},
		indexNorm: {
			value: 'maxmin',
			learn: false
		},
		valueNorm: {
			value: 'maxmin',
			learn: false
		},
		activNorm: {
			value: 'maxmin',
			learn: false
		},
		activImpl: {
			value: 'aliev4',
			learn: false
		},
		activNorm2: {
			value: 'maxmin',
			learn: false
		},
		defMethod: {
			value: 'cog'
		}
	},
	inputVariables: [],
	outputVariables: [],
	rules: [],
	trainingSet: [],
	testingSet: [],
	training: {
		mu: 250,
		lambda: 250 * 4,
		rounds: 20,
		samples: 1024 // 48 * 1024 / 4
	},
	caseSet: []
};

class System extends React.Component
{
	static propTypes = {
		system: PropTypes.shape({
			id: PropTypes.number.isRequired,
			name: PropTypes.string,
			createdAt: PropTypes.string.isRequired,
			updatedAt: PropTypes.string.isRequired
		}).isRequired,
		onNameUpdate: PropTypes.func.isRequired,
		onClick: PropTypes.func.isRequired
	}

	constructor(props)
	{
		super(props);

		this.state = {name: this.props.system.name};
	}

	componentWillReceiveProps(newProps)
	{
		this.setState({name: newProps.system.name});
	}

	updateName = () =>
	{
		if (this.state.name !== this.props.system.name)
			this.props.onNameUpdate(this.state.name);
	}

	render()
	{
		const {system} = this.props;

		return (
			<ListGroupItem action onClick={this.props.onClick}>
				<Row style={{display: 'flex', alignItems: 'center'}}>
					<Col xl={1} lg={2} md={2} style={{width: '100%', textAlign: 'end'}}>
						<span style={{fontSize: '22px'}}>
							#{system.id}
						</span>
					</Col>
					<Col xl={8} lg={6} md={10}>
						<Input
							style={{
								fontSize: '24px',
								// backgroundColor: '#eee',
								// backgroundColor: 'transparent',
								boxShadow: 'none',
								border: 'none',
								padding: '0 0.5em'
							}}
							value={this.state.name}
							placeholder="Нет названия"
							onBlur={this.updateName}
							onChange={(e) => this.setState({name: e.target.value})}
							onClick={(e) => e.stopPropagation()}
						/>
					</Col>
					<Col xl={3} lg={4} md={12}>
						<div
							className="mt-3 mt-lg-0 d-block d-sm-flex d-lg-block"
							style={{justifyContent: 'space-between', textAlign: 'end'}}
						>
							<span>
								Создано
								{' '}
								<span id={`created-${system.id}`} className="questionable">
									{ellipsize(moment.duration(moment(system.createdAt).diff(moment())).humanize(true), 16)}
								</span>
							</span>
							<br className="d-lg-block d-sm-none d-block" />
							<span>
								Изменено
								{' '}
								<span id={`updated-${system.id}`} className="questionable">
									{ellipsize(moment.duration(moment(system.updatedAt).diff(moment())).humanize(true), 16)}
								</span>
							</span>
						</div>
						<UncontrolledTooltip
							target={`created-${system.id}`}
							placement="left"
							// delay={{show: 0, hide: 0}}
						>
							{moment(system.createdAt).format('DD.MM.YY HH:mm:ss')}
						</UncontrolledTooltip>
						<UncontrolledTooltip
							target={`updated-${system.id}`}
							placement="left"
							// delay={{show: 0, hide: 0}}
						>
							{moment(system.updatedAt).format('DD.MM.YY HH:mm:ss')}
						</UncontrolledTooltip>
					</Col>
				</Row>
			</ListGroupItem>
		);
	}
}

class SystemSelector extends React.Component
{
	static propTypes = {
		isOpen: PropTypes.bool.isRequired,
		onExit: PropTypes.func.isRequired,
		username: PropTypes.string.isRequired
	}

	static contextType = NotificationContext

	state = {
		pending: true,
		systems: [],
		editing: null,
		editorIsOpen: false,
		editorValues: {}
	}

	componentDidMount = () =>
	{
		if (this.props.isOpen)
			this.fetch();
	}

	componentWillReceiveProps = (newProps) =>
	{
		if (!this.props.isOpen && newProps.isOpen)
			this.fetch();
	}

	fetch = () =>
	{
		this.setState({pending: true});

		return axios.get('/systems')
			.then(({data}) => this.setState({pending: false, systems: data}))
			.catch(err => this.context(
				'danger', 'Нейро-нечеткие системы', `Ошибка запроса систем: ${err.message}`
			));
	}

	createSystem = () =>
	{
		this.setState({pending: true});

		return axios.post('/systems', {name: '', definition: DEFAULT_SYSTEM})
			.then(({data}) =>
			{
				this.setState({pending: false});
				this.context('success', 'Нейро-нечеткие системы', `Создана система #${data}`);
			})
			.catch(err => this.context(
				'danger', 'Нейро-нечеткие системы', `Ошибка создания системы: ${err.message}`
			))
			.then(this.fetch);
	}

	updateSystem = (id) => (values) =>
	{
		this.setState({pending: true});

		return axios.patch(`/systems/${id}`, values)
			.then(() =>
			{
				this.setState({pending: false});
				this.context('success', 'Нейро-нечеткие системы', `Система #${id} успешно сохранена`);
			})
			.catch(err => this.context(
				'danger', 'Нейро-нечеткие системы', `Ошибка сохранения системы #${id}: ${err.message}`
			))
			.then(this.fetch);
	}

	editSystem = (id) => () =>
	{
		this.setState({pending: true});

		return axios.get(`/systems/${id}`)
			.then(({data}) =>
			{
				this.setState({
					pending: false,
					editing: id,
					editorIsOpen: true,
					editorValues: data
				});
			})
			.catch(err => this.context(
				'danger', 'Нейро-нечеткие системы', `Ошибка запроса системы #${id}: ${err.message}`
			))
			.then(this.fetch);
	}

	saveSystem = (definition) =>
		this.updateSystem(this.state.editing)({definition})

	copySystem = (definition) =>
	{
		const id = this.state.editing;

		return axios.post('/systems', {
			name: `Копия ${this.state.editorValues.name || `#${id}`}`,
			definition
		}).then(({data}) => {
			this.context('success', 'Нейро-нечеткие системы', `Система #${id} сохранена как #${data}`);
		}).catch((err) => this.context(
			'danger', 'Нейро-нечеткие системы', `Ошибка копирования системы #${id}: ${err.message}`
		)).then(this.fetch);
	}

	dropSystem = () =>
	{
		const id = this.state.editing;

		return axios.delete(`/systems/${id}`)
			.then(() =>
			{
				this.setState({editorIsOpen: false});
				this.context('success', 'Нейро-нечеткие системы', `Система #${id} удалена`);
			})
			.catch((err) => this.context(
				'danger', 'Нейро-нечеткие системы', `Ошибка удаления системы #${id}: ${err.message}`
			))
			.then(this.fetch);
	}

	// onChange = name => ev => this.setState({...this.state, [name]: ev.target.value})

	// withVals = func => () => func(this.state.username, this.state.password)

	render()
	{
		return (
			<React.Fragment>
				<Modal
					id="system-selector-modal"
					backdrop="static"
					isOpen={this.props.isOpen}
					// toggle={this.props.toggle}
					centered
				>
					<ModalHeader style={{display: 'flex', justifyItems: 'space-between'}}>
						Нейро-нечеткие системы
					</ModalHeader>
					<ModalBody className="p-0">
						{this.renderBody()}
					</ModalBody>
					<ModalFooter>
						<div style={{whiteSpace: 'nowrap'}}>
							Выполнен вход от имени <strong>{this.props.username}</strong>
						</div>
						<div className="w-100" />
						<Button
							color="secondary"
							style={{whiteSpace: 'nowrap'}}
							onClick={this.props.onExit}
							// disabled={locked}
						>
							Выход
						</Button>
					</ModalFooter>
				</Modal>
				<SystemEditor
					isOpen={this.state.editorIsOpen}
					values={this.state.editorValues.definition}
					onExit={() => this.setState({editorIsOpen: false})}
					onSave={this.saveSystem}
					onCopy={this.copySystem}
					onDrop={this.dropSystem}
					name={this.state.editorValues.name || `Система #${this.state.editing}`}
				/>
			</React.Fragment>
		);
	}

	renderBody()
	{
		return (
			<React.Fragment>
				<div style={{height: '429px', overflowY: 'scroll'}}>
					<ListGroup className="p-3">
						{this.state.systems.map(system => (
							<System
								key={system.id}
								system={system}
								onNameUpdate={name => this.updateSystem(system.id)({name})}
								onClick={this.editSystem(system.id)}
							/>
						))}
					</ListGroup>
				</div>
				<div className="border-top" style={{}}>
					<Button
						className="m-3"
						style={{width: 'calc(100% - 2rem)'}}
						color="primary"
						onClick={this.createSystem}
					>
						Создать систему
					</Button>
				</div>
				{this.state.pending && (
					<div style={{position: 'absolute', top: 0, width: '100%', height: '100%', backgroundColor: 'rgba(255, 255, 255, 0.5)'}}>
						<div style={{position: 'absolute', top: '50%', left: '50%', transform: 'translateX(-50%), translateY(-50%)'}}>
							<Spinner />
						</div>
					</div>
				)}
			</React.Fragment>
		);
	}
}

module.exports = SystemSelector;
