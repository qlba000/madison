const React = require('react');
const {default: ReactSelect, components} = require('react-select');
const _ = require('lodash');

module.exports = function Select({name, options, onChange, value, ...props})
{
	return (
		<ReactSelect
			classNamePrefix="react-select"
			className="react-select"
			// components={{Option: (props) => <components.Option {...props} />}}
			id={`${name}Select`}
			name={`${name}Select`}
			options={options}
			onChange={(option) => onChange(option.value)}
			value={_.find(options, {value})}
			{...props}
		/>
	);
};
