const _ = require('lodash');
const React = require('react');
const PropTypes = require('prop-types');
const {
	Alert,
	Button: BaseButton,
	Col,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Row,
	UncontrolledTooltip
} = require('reactstrap');
const {
	ResponsiveContainer,
	LineChart,
	Line,
	CartesianGrid,
	XAxis,
	YAxis,
	Tooltip
} = require('recharts');
const Progress = require('react-circular-progressbar').default;


function Button(props)
{
	return (
		<BaseButton
			{...props}
			{...props.disabled && {color: 'secondary'}}
		>
			{props.children}
		</BaseButton>
	);
}

class TrainingModal extends React.Component
{
	static propTypes = {
		connected: PropTypes.bool.isRequired,
		round: PropTypes.number.isRequired,
		chart: PropTypes.arrayOf(PropTypes.shape({
			round: PropTypes.number.isRequired,
			best: PropTypes.number.isRequired,
			aver: PropTypes.number.isRequired,
			disp: PropTypes.number.isRequired
		}).isRequired).isRequired,
		state: PropTypes.oneOf(['standBy', 'running', 'success', 'failure']).isRequired,
		error: PropTypes.string.isRequired,
		rounds: PropTypes.number.isRequired,
		isOpen: PropTypes.bool.isRequired,
		onStop: PropTypes.func.isRequired,
		onClose: PropTypes.func.isRequired,
		onCommit: PropTypes.func.isRequired
	}

	onStopClick = () => this.props.onStop();

	render()
	{
		return (
			<Modal
				id="training-modal"
				backdrop="static"
				isOpen={this.props.isOpen}
				// toggle={this.props.onClose}
				centered
				className="wide-modal"
			>
				<ModalHeader>
					Обучение
				</ModalHeader>
				<ModalBody>
					{this.renderBody()}
				</ModalBody>
				<ModalFooter>
					<div
						id="indicator"
						style={{
							height: '100%',
							marginRight: '4px',
							backgroundColor: '#000',
							borderRadius: '5px'
						}}
					>
						<div
							style={{
								transition: '1.5s',
								width: '15px',
								height: '15px',
								margin: '11px',
								borderRadius: '50%',
								boxShadow: this.props.connected ? '0 0 10px #0f0, 0 0 30px #0f0' : '0 0 0 #333',
								backgroundColor: this.props.connected ? '#3f3' : '#333'
							}}
						/>
					</div>
					<UncontrolledTooltip
						placement="bottom"
						target="indicator"
					>
						Соединение {this.props.connected ? 'установлено' : 'закрыто'}
					</UncontrolledTooltip>
					<div className="d-none d-md-block w-100">
						{this.renderAlert()}
					</div>
					<div style={{width: '100%'}} className="d-block d-md-none" />
					<Button
						color="danger"
						onClick={this.props.onStop}
						disabled={_(['success', 'failure']).includes(this.props.state)}
					>
						Остановить
					</Button>
					<Button
						color="danger"
						onClick={this.props.onClose}
						disabled={_(['standBy', 'running']).includes(this.props.state)}
					>
						Отмена
					</Button>
					<Button
						id="training-apply"
						color="primary"
						onClick={this.props.onCommit}
						disabled={this.props.state !== 'success'}
					>
						Применить
					</Button>
					<UncontrolledTooltip
						placement="left"
						target="training-apply"
					>
						Изменить значения параметров в соответствии с результатом обучения
					</UncontrolledTooltip>
				</ModalFooter>
			</Modal>
		);
	}

	renderBody()
	{
		const progressStyles = {
			standBy: {
				main: '#00e2',
				back: '#00e2'
			},
			running: {
				main: '#00e',
				back: '#00e2'
			},
			success: {
				main: '#0e0',
				back: '#0e02'
			},
			failure: {
				main: '#e00',
				back: '#e002'
			}
		};

		const {round, state} = this.props, off = round === null;

		return (
			<Row>
				<Col md={3} className="d-none d-md-block">
					<Progress
						percentage={off ? 100 : 100 * round / this.props.rounds}
						text={off ? '--' : round}
						background
						backgroundPadding={6}
						strokeWidth={4}
						styles={{
							background: {fill: 'transparent'},
							text: {fill: progressStyles[state].main},
							path: {stroke: progressStyles[state].main},
							trail: {stroke: progressStyles[state].back}
						}}
					/>
				</Col>
				<Col md={9}>
					<ResponsiveContainer width="100%" aspect={10 / 3}>
						<LineChart
							data={this.props.chart}
							// margin={{top: 5, right: 30, left: 20, bottom: 5}}
							margin={0}
						>
							<CartesianGrid strokeDasharray="3 3" />
							<XAxis type="number" dataKey="round" domain={[1, this.props.rounds]} />
							<YAxis type="number" yAxisId="left" orientation="left" />
							<YAxis type="number" yAxisId="right" orientation="right" />
							<Tooltip />
							<Line type="monotone" dataKey="best" name="Лучший" yAxisId="left" dot={false} stroke="#0e0" animationDuration={0.3} />
							<Line type="monotone" dataKey="aver" name="Средний" yAxisId="left" dot={false} stroke="#00e" animationDuration={0.3} />
							<Line type="monotone" dataKey="disp" name="СКО (прав. ось)" yAxisId="right" dot={false} stroke="#e00" animationDuration={0.3} />
						</LineChart>
					</ResponsiveContainer>
				</Col>
				<Col sm={12} className="d-block d-md-none">
					{this.renderAlert()}
				</Col>
			</Row>
		);
	}

	renderAlert()
	{
		const stateHash = {
			standBy: {
				color: 'warning',
				error: 'Ожидание запуска...'
			},
			running: {
				color: 'primary',
				error: 'Выполняется обучение...'
			},
			success: {
				color: 'success',
				error: 'Обучение успешно завершено'
			},
			failure: {
				color: 'danger',
				error: 'Произошла неизвестная ошибка'
			}
		};

		return (
			<Alert
				color={stateHash[this.props.state].color}
				style={{
					marginBottom: 0,
					padding: '6px 12px',
					width: '100%'
				}}
			>
				{this.props.error || stateHash[this.props.state].error || null}
			</Alert>
		);
	}
}

module.exports = TrainingModal;
