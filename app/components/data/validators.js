const _ = require('lodash');

exports.required = () => value =>
	(value !== '' ? 0 : 'Введите значение');

exports.number = () => value =>
	(_.isFinite(Number(value)) ? 0 : 'Значение должно быть числом');

exports.domain = () => (value, {xMin, xMax}) =>
	(xMin <= +value && +value <= xMax ? 0 : `Значение должно принадлежать отрезку [${xMin}; ${xMax}]`);

exports.range = (min, max) => () => value =>
	(+min <= +value && +value <= +max ? 0 : `Значение должно принадлежать отрезку [${min}; ${max}]`);

exports.le = (what, whatLabel) => (value, values) =>
	(+value <= +values[what] ? 0 : `Значение не должно превышать ${whatLabel}`);

exports.ge = (what, whatLabel) => (value, values) =>
	(+value >= +values[what] ? 0 : `Значение не может быть меньше ${whatLabel}`);
