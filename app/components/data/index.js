const _ = require('lodash');

const {required, number, domain, range} = require('./validators'); // , ...validators

// const le = passLabel(validators.le);
// const ge = passLabel(validators.ge);

const data = exports.data = {
	delta: {
		caption: 'Синглтон (дискретная \u03B4-функция)',
		params: {
			deltaX: {
				label: 'x\u2080',
				title: 'Абсцисса ненулевого значения',
				tests: [domain]
			}
		},
		func: ({deltaX: {value: x0}}, xMin, xMax) => (xMin <= x0 && x0 < xMax ? 1 : 0)
	},
	gauss: {
		caption: 'Гауссова',
		params: {
			gaussMu: {
				label: '\u03BC',
				title: 'Мат. ожидание, центр',
				tests: [domain]
			},
			gaussSigma: {
				label: '\u03C3',
				title: 'Среднеквадратическое отклонение',
				tests: [range(0, Infinity)]
			}
		},
		func: ({gaussMu: {value: mu}, gaussSigma: {value: sigma}}, xMin, xMax) =>
			Math.max(
				Math.exp(-Math.pow((xMin - mu) / sigma, 2)),
				Math.exp(-Math.pow((xMax - mu) / sigma, 2)),
				xMin <= mu && mu < xMax ? 1 : 0
			)
	}
	// bell: {
	// 	caption: 'Bell (generalized)',
	// 	params: {
	// 		bellMu: {
	// 			label: '\u03BC',
	// 			tests: [domain]
	// 		},
	// 		bellSigma: {
	// 			label: '\u03C3',
	// 			tests: [range(0, Infinity)]
	// 		},
	// 		bellAlpha: {
	// 			label: '\u03B1',
	// 			tests: [range(0, Infinity)]
	// 		}
	// 	},
	// 	func: ({bellMu: {value: mu}, bellSigma: {value: sigma}, bellAlpha: {value: alpha}}, xMin, xMax) =>
	// 		Math.max(
	// 			0,
	// 			1 / (1 + Math.pow(Math.abs((xMin - mu) / sigma), 2 * alpha)),
	// 			1 / (1 + Math.pow(Math.abs((xMax - mu) / sigma), 2 * alpha)),
	// 			xMin <= mu && mu < xMax ? 1 : 0
	// 		)
	// },
	// spike: {
	// 	caption: 'Spike',
	// 	params: {
	// 		spikeMu: {
	// 			label: '\u03BC',
	// 			tests: [domain]
	// 		},
	// 		spikeSigma: {
	// 			label: '\u03C3',
	// 			tests: [range(0, Infinity)]
	// 		},
	// 		spikeAlpha: {
	// 			label: '\u03B1',
	// 			tests: [range(0, Infinity)]
	// 		}
	// 	},
	// 	func: ({spikeMu: {value: mu}, spikeSigma: {value: sigma}, spikeAlpha: {value: alpha}}, xMin, xMax) =>
	// 		Math.max(
	// 			0,
	// 			1 - Math.pow(Math.abs(xMin - mu) / sigma, alpha / 2),
	// 			1 - Math.pow(Math.abs(xMax - mu) / sigma, alpha / 2),
	// 			xMin <= mu && mu < xMax ? 1 : 0
	// 		)
	// },
	// triangle: {
	// 	caption: 'Triangular',
	// 	params: {
	// 		trianA: {
	// 			label: 'a',
	// 			tests: [domain, le('trianB'), le('trianC')]
	// 		},
	// 		trianB: {
	// 			label: 'b',
	// 			tests: [domain, ge('trianA'), le('trianC')]
	// 		},
	// 		trianC: {
	// 			label: 'c',
	// 			tests: [domain, ge('trianA'), ge('trianB')]
	// 		}
	// 	},
	// 	func: ({trianA: {value: a}, trianB: {value: b}, trianC: {value: c}}, xMin, xMax) =>
	// 		Math.max(
	// 			0,
	// 			xMax <= b ? (xMax - a) / (b - a) : 0,
	// 			xMin >= b ? (xMin - c) / (b - c) : 0,
	// 			xMin <= b && b < xMax ? 1 : 0
	// 		)
	// }
};

// function passLabel(func)
// {
// 	return what => () =>
// 	{
// 		for (const type in data)
// 			if (data[type].params[what])
// 				return func(what, data[type].params[what].label);
// 	};
// }


const validator = ({type, xMin, xMax, ...otherValues}) =>
{
	const errors = {}, values = {xMin, xMax};

	for (const param in otherValues)
		values[param] = otherValues[param].value;

	for (const param in data[type].params)
	{
		const {tests} = data[type].params[param];

		for (const test of [required, number, ...tests])
		{
			const result = test()(values[param], values);

			if (result)
			{
				errors[param] = {value: result};
				break;
			}
		}
	}

	return errors;
};

exports.formikConfig =
{
	mapPropsToValues: props =>
	{
		const result = {
			type: _(data).keys().first(),
			xMin: props.xMin,
			xMax: props.xMax
		};

		_(data).each(({params}) => _(params).keys().each(param =>
		{
			result[param] = {value: '', learn: false};
		}));

		if (props.values) {
			const {type, ...params} = props.values;

			result.type = type;

			_(params).each(({value, learn}, param) =>
			{
				result[param] = {value: String(value), learn};
			});
		}

		return result;
	},
	handleSubmit: (values, {setSubmitting, props}) =>
	{
		setSubmitting(false);

		const result = _(values)
			.pick(..._.keys(data[values.type].params))
			.mapValues(({value, learn}) => ({value: Number(value), learn}))
			.value();

		result.type = values.type;

		props.onSubmit(result);
	},
	validate: validator,
	isInitialValid: ({values}) => Boolean(values)
};
