const React = require('react');
const PropTypes = require('prop-types');
const {
	Button,
	ModalHeader,
	ModalBody,
	ModalFooter
} = require('reactstrap');
const formik = require('formik');
const {formikConfig} = require('../data');
const FormContent = require('./FormContent');

function ModalContent({onClose, ...props})
{
	const onSubmitClick = () =>
	{
		props.submitForm().then(onClose);
	};

	return (
		<React.Fragment>
			<ModalHeader>
				Функция принадлежности
			</ModalHeader>
			<ModalBody>
				<FormContent {...props} />
			</ModalBody>
			<ModalFooter>
				<Button color="secondary" onClick={onClose}>
					Отмена
				</Button>
				<Button color="primary" onClick={onSubmitClick} disabled={!props.isValid}>
					ОК
				</Button>
			</ModalFooter>
		</React.Fragment>
	);
}

ModalContent.propTypes = {
	onClose: PropTypes.func.isRequired,
	submitForm: PropTypes.func.isRequired,
	isValid: PropTypes.bool.isRequired,
	values: PropTypes.any.isRequired
};

module.exports = formik.withFormik(formikConfig)(ModalContent);
