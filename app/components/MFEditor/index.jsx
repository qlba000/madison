const React = require('react');
const PropTypes = require('prop-types');
const {Modal} = require('reactstrap');
const ModalContent = require('./ModalContent');

function MFEditor({isOpen, ...props})
{
	return (
		<Modal
			id="mfeditor-modal"
			className="wide-modal"
			backdrop="static"
			centered
			isOpen={isOpen}
			// toggle={props.onClose}
		>
			<ModalContent {...props} />
		</Modal>
	);
}

MFEditor.propTypes = {
	isOpen: PropTypes.bool.isRequired,
	onClose: PropTypes.func.isRequired
};

module.exports = MFEditor;
