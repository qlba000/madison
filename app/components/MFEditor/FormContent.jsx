const _ = require('lodash');
const React = require('react');
const PropTypes = require('prop-types');
const {
	Col,
	Form,
	FormGroup,
	Label,
	Row
} = require('reactstrap');
const {Area} = require('recharts');
const Select = require('../Select');
const Chart = require('../Chart');
const ParamInput = require('../ParamInput.jsx');
const {data} = require('../data');
const {tabulateChart} = require('../utils');

const options = _.map(data, (value, key) => ({value: key, label: value.caption}));

class FormContent extends React.Component
{
	constructor(props)
	{
		super(props);

		this.tabulateChart = () => tabulateChart(
			data[this.props.values.type].func,
			this.props.values,
			this.props.xMin,
			this.props.xMax,
			this.props.isValid
		);
		this.updateChart = _.debounce(() => this.setState({chartPoints: this.tabulateChart()}), 750);

		this.state = {chartPoints: this.tabulateChart()};
	}

	componentWillReceiveProps()
	{
		this.updateChart();
	}

	componentWillUnmount() {
		this.setState = () => {};
	}

	render()
	{
		const {noLearn, values, setFieldValue, errors} = this.props;

		return (
			<Row>
				<Col mediaGroup={5} className="border-right">
					<Form>
						<FormGroup row>
							<Label htmlFor="type" sm="2">Тип</Label>
							<Col sm="10">
								<Select
									name="type"
									options={options}
									onChange={(value) => setFieldValue('type', value)}
									value={values.type}
								/>
							</Col>
						</FormGroup>
						<hr />
						{_.map(data[values.type].params, ({label, title}, key) => (
							<ParamInput
								name={key}
								label={<abbr title={title}>{label}</abbr>}
								noLearn={noLearn}
								values={values}
								errors={errors}
								setFieldValue={setFieldValue}
							/>
						))}
					</Form>
				</Col>
				<Col md={7}>
					<Chart
						xMin={this.props.xMin}
						xMax={this.props.xMax}
					>
						<Area
							data={this.state.chartPoints}
							dataKey="y"
							stroke="rgba(53, 253, 97, 1)"
							fill="rgba(53, 253, 97, 0.3)"
						/>
					</Chart>
				</Col>
			</Row>
		);
	}
}

FormContent.propTypes = {
	noLearn: PropTypes.bool,
	values: PropTypes.any.isRequired,
	setFieldValue: PropTypes.func.isRequired,
	errors: PropTypes.any.isRequired,
	isValid: PropTypes.bool.isRequired,
	xMin: PropTypes.number.isRequired,
	xMax: PropTypes.number.isRequired
};

FormContent.defaultProps = {
	noLearn: false
};

module.exports = FormContent;
