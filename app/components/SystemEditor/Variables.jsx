const _ = require('lodash');
const React = require('react');
const {
	Button,
	Col,
	DropdownItem,
	DropdownMenu,
	DropdownToggle,
	Input,
	ListGroup,
	ListGroupItem,
	Row,
	UncontrolledDropdown,
	UncontrolledTooltip
} = require('reactstrap');
const {Area} = require('recharts');
const PropTypes = require('prop-types');
const cc = require('color-convert');
const {data} = require('../data');
const LVEditor = require('../LVEditor');
const Chart = require('../Chart');
const {tabulateChart} = require('../utils');


class Variables extends React.Component
{
	state = {
		isLVEditorOpen: false,
		LVEditorValues: null,
		activeVariable: null,
		variableName: ''
	}

	setActiveVariable = name =>
	{
		this.setState({activeVariable: name, variableName: name || ''});
	}

	mangleActiveVariable = (action, delta) =>
	{
		const variableIndex = _.findIndex(this.props.values, {name: this.state.activeVariable});
		const newState = _.cloneDeep(this.state);
		const newValue = _.cloneDeep(this.props.values);

		switch (action)
		{
		case 'SAVE':
			if (variableIndex < 0)
				newValue.push({name: this.state.variableName, ...delta, chart: null});
			else
				newValue[variableIndex] = {...newValue[variableIndex], ...delta, chart: null};

			newState.activeVariable = this.state.variableName;

			this.props.onChange(newValue);
			break;

		case 'EDIT':
			newState.LVEditorValues = newValue[variableIndex].specs;
			newState.isLVEditorOpen = true;
			break;

		case 'REMOVE':
			newValue.splice(variableIndex, 1);
			newState.activeVariable = null;
			newState.variableName = '';

			this.props.onChange(newValue);
			break;

		default:
			return;
		}

		this.setState(newState);
	}

	isVariableNameValid = (name) => name !== '' && !_.find(this.props.values, {name})

	prepareVariableChart = variable => variable.chart || (variable.chart = variable.specs.terms.map(term => tabulateChart(
		data[term.mf.type].func,
		term.mf,
		Number(variable.specs.xMin),
		Number(variable.specs.xMax),
		true
	)));

	render()
	{
		return (
			<React.Fragment>
				<Row>
					<Col xs={4} className="border-right">
						{this.renderVariableList()}
					</Col>
					<Col xs={8}>
						{this.renderVariableMenu()}
						<hr />
						<Row className="pr-2">
							{this.renderChart()}
						</Row>
					</Col>
				</Row>
				<LVEditor
					name={this.state.variableName || ''}
					handleSubmit={specs => this.mangleActiveVariable('SAVE', {specs})}
					onClose={() => this.setState({isLVEditorOpen: false})}
					values={this.state.LVEditorValues}
					isOpen={this.state.isLVEditorOpen}
				/>
			</React.Fragment>
		);
	}

	renderVariableList()
	{
		const {values, maxCount} = this.props;

		return (
			<ListGroup>
				{values.map(({name}) => (
					<ListGroupItem
						action
						active={this.state.activeVariable === name}
						onClick={() => this.setActiveVariable(name)}
					>
						{name}
					</ListGroupItem>
				))}
				{(!maxCount || values.length < maxCount) && (
					<ListGroupItem
						action
						active={this.state.activeVariable === null}
						onClick={() => this.setActiveVariable(null)}
					>
						Новая переменная
					</ListGroupItem>
				)}
			</ListGroup>
		);
	}

	renderVariableMenu()
	{
		const {variableName, activeVariable} = this.state;

		return (
			<div style={{display: 'flex'}} className="mb-2">
				<Input
					type="text"
					id="variableName"
					name="variableName"
					placeholder={activeVariable === null ? 'Название новой переменной' : 'Новое название переменной'}
					onChange={({target}) => this.setState({variableName: target.value})}
					value={variableName}
					style={{flexGrow: 1, flexShrink: 1, minWidth: 0}}
				/>
				{activeVariable === null && this.isVariableNameValid(variableName) && (
					<Button
						className="ml-2"
						color="primary"
						onClick={() => this.setState({
							isLVEditorOpen: true,
							LVEditorValues: null
						})}
					>
						Создать
					</Button>
				)}
				{activeVariable !== null && (
					<UncontrolledDropdown>
						<DropdownToggle caret color="primary" className="ml-2">
							Действия
						</DropdownToggle>
						<DropdownMenu right>
							<DropdownItem
								id="rename-var"
								// disabled={!this.isVariableNameValid(variableName)}
								onClick={() =>
									this.isVariableNameValid(variableName) &&
									this.mangleActiveVariable('SAVE', {name: this.state.variableName})
								}
								// className="text-warning"
							>
								Переименовать
							</DropdownItem>
							{(variableName === activeVariable) && (
								<UncontrolledTooltip
									target="rename-var"
									placement="left"
									delay={0}
								>
									Измените имя переменной в поле ввода
								</UncontrolledTooltip>
							)}
							<DropdownItem
								onClick={() => this.mangleActiveVariable('EDIT')}
								// className="text-info"
							>
								Редактировать
							</DropdownItem>
							<DropdownItem
								onClick={() => this.mangleActiveVariable('REMOVE')}
								// className="text-danger"
							>
								Удалить
							</DropdownItem>
						</DropdownMenu>
					</UncontrolledDropdown>
				)}
			</div>
		);
	}

	renderChart()
	{
		const variable = _.find(this.props.values, {name: this.state.activeVariable});

		return (
			<Chart
				xMin={variable ? variable.specs.xMin : 0}
				xMax={variable ? variable.specs.xMax : 0}
			>
				{variable && this.prepareVariableChart(variable).map((table, index) =>
				{
					const color = `#${cc.hwb.hex((index * 30 + 90) % 360, 0, 0)}`;

					return (
						<Area
							name={variable.name}
							data={table}
							dataKey="y"
							stroke={color}
							fill={color}
							fillOpacity={0.3}
						/>
					);
				})}
			</Chart>
		);
	}
}

module.exports = Variables;
