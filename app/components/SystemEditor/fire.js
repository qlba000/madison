const _ = require('lodash');
const axios = require('axios').default;
const io = require('socket.io-client');
const cleaner = require('deep-cleaner');

exports.fire = (
	systemDefinition,
	params,
	options,
	onConnect,
	onDisconnect,
	onError,
	onProgress,
	onResult
) =>
	axios.post('/fire', {
		systemDefinition: clear(_.cloneDeep(systemDefinition)),
		params,
		options
	}).then(response =>
	{
		const socket = io.connect(`/${response.data.nsp}`, {path: '/ws'});

		socket.on('connect', onConnect);
		socket.on('disconnect', onDisconnect);
		socket.on('message', (message) =>
		{
			if (_.has(message, 'error'))
				onError(message.error);

			if (_.has(message, 'round'))
				onProgress({
					...message,
					round: message.round + 1
				});

			if (_.has(message, 'result'))
				onResult(message.result);
		});

		return socket;
	}).catch(onError);

exports.test = (
	systemDefinition,
	paramsVector,
	samplesCount
) => new Promise((resolve, reject) => exports.fire(
	systemDefinition,
	{paramsVectors: [paramsVector]},
	{maxSamples: samplesCount},
	_.noop,
	_.noop,
	reject,
	_.noop,
	resolve
));


function clear(systemDefinition)
{
	return cleaner(systemDefinition, ['chart', 'table', 'key']);
}
