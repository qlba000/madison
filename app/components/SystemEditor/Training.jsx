const _ = require('lodash');
const axios = require('axios').default;
const React = require('react');
const {
	Button,
	Col,
	FormGroup,
	Input,
	Label,
	Row,
	UncontrolledTooltip
} = require('reactstrap');
const PropTypes = require('prop-types');
const {fire, test} = require('./fire');
const TrainingModal = require('../TrainingModal');
const TestingModal = require('../TestingModal');

const TippedInput = require('../Tipped')('bottom')(Input);

function Param({values, errors, onChange, name, caption})
{
	return (
		<Col lg={3} sm={6}>
			<FormGroup>
				<Label for={name}>{caption}</Label>
				<TippedInput
					id={name}
					name={name}
					error={errors[name]}
					value={values[name]}
					onChange={e => onChange(name, e.target.value)}
				/>
			</FormGroup>
		</Col>
	);
}

class Training extends React.Component
{
	static propTypes = {
		values: PropTypes.shape({
			mu: PropTypes.string.isRequired,
			lambda: PropTypes.string.isRequired,
			rounds: PropTypes.string.isRequired,
			samples: PropTypes.string.isRequired
		}).isRequired,
		systemDefinition: PropTypes.any.isRequired,
		onChange: PropTypes.func.isRequired,
		onCommit: PropTypes.func.isRequired
	}

	state = {
		isTrainingModalOpen: false,
		trainingState: 'failure'
	}

	onFire = () =>
	{
		this.setState({
			isTrainingModalOpen: true,
			trainingConnected: false,
			trainingState: 'standBy',
			trainingRound: null,
			trainingChart: [],
			trainingError: null,
			trainingResult: null,
			isTestingModalOpen: false,
			testingError: null,
			testingChart: null,
			testingFitness: null
		});

		const {samples, ...params} = _.mapValues(this.props.values, Number);

		fire(
			this.props.systemDefinition,
			params,
			{maxSamples: samples},
			() => this.setState({
				trainingConnected: true,
				trainingState: 'running',
				trainingRound: 0
			}),
			this.onTrainingEnd,
			(error) => this.setState({
				trainingState: 'failure',
				trainingError: error.message
			}),
			(message) => this.setState({
				trainingRound: message.round,
				trainingChart: [
					...this.state.trainingChart, message
				]
			}),
			(result) => this.setState({
				trainingResult: result,
				trainingState: 'success',
				trainingError: 'Обучение успешно завершено'
			})
		).then(socket => this.socket = socket);
	}

	onStop = () =>
	{
		if (this.socket.connected)
		{
			this.socket.emit('stop');

			this.setState({
				trainingState: 'failure',
				trainingError: 'Обучение прервано'
			});
		}
	}

	onTrainingEnd = () =>
	{
		this.setState({
			trainingConnected: false,
			..._.includes(['standBy', 'running'], this.state.trainingState) && ({
				trainingState: 'failure',
				trainingError: 'Соединение потеряно'
			})
		});

		if (this.state.trainingState !== 'success' ||
			!this.state.trainingResult ||
			!this.props.systemDefinition.testingSet.length)
			return;

		this.setState({isTestingModalOpen: true});

		const samplesCount = Number(this.props.values.samples);

		test(
			{
				...this.props.systemDefinition,
				trainingSet: this.props.systemDefinition.testingSet
			},
			this.state.trainingResult.values,
			samplesCount
		).then(result =>
		{
			const chart = _.zipWith(
				_.map(this.props.systemDefinition.testingSet, 'output'),
				result.values,
				(expd, recv) => ({expected: expd, received: recv})
			);

			this.setState({
				testingChart: chart,
				testingFitness: Math.sqrt(_.sumBy(
					chart,
					({expected, received}) => (expected - received) ** 2
				) / this.props.systemDefinition.testingSet.length)
			});
		}).catch(error =>
		{
			this.setState({
				testingError: error.message
			});
		});
	}

	onCommit = () =>
	{
		const {params, values} = this.state.trainingResult;

		this.props.onCommit(_.zipObject(params, values));
		this.onClose();
	}

	onClose = () =>
	{
		this.setState({isTrainingModalOpen: false});
	}

	onTestingModalClose = () =>
	{
		this.setState({isTestingModalOpen: false});
	}

	render()
	{
		if (!this.props.systemDefinition.outputVariables[0]) return (
			<div
				style={{
					height: '100%',
					display: 'flex',
					flexDirection: 'column',
					justifyContent: 'space-around',
					alignSelf: 'center'
				}}
			>
				<p style={{fontSize: '32px', color: 'grey', textAlign: 'center'}}>Нет выходной переменной</p>
			</div>
		);

		const {values, onChange} = this.props;

		const common = {
			values,
			errors: validate(values),
			onChange: (name, value) => onChange({...values, [name]: value})
		};

		return (
			<React.Fragment>
				<Row>
					<Param key={0} {...common} caption={'\u03bc'} name="mu" />
					<Param key={1} {...common} caption={'\u03bb'} name="lambda" />
					<Param key={2} {...common} caption="Кол-во поколений" name="rounds" />
					<Param key={3} {...common} caption="Кол-во отсчетов" name="samples" />
				</Row>

				<Row>
					<Col xs={{size: 4, offset: 4}}>
						<Button block color="danger" className="mt-4" onClick={this.onFire}>
							Запуск
						</Button>
					</Col>
				</Row>

				<TrainingModal
					connected={this.state.trainingConnected}
					isOpen={this.state.isTrainingModalOpen}
					onCommit={this.onCommit}
					onClose={this.onClose}
					onStop={this.onStop}
					round={this.state.trainingRound}
					chart={this.state.trainingChart}
					state={this.state.trainingState}
					error={this.state.trainingError}
					rounds={Number(values.rounds)}
				/>

				<TestingModal
					error={this.state.testingError}
					chart={this.state.testingChart}
					chartRange={[
						this.props.systemDefinition.outputVariables[0].specs.xMin,
						this.props.systemDefinition.outputVariables[0].specs.xMax
					]}
					fitness={this.state.testingFitness}
					isOpen={this.state.isTestingModalOpen}
					onClose={this.onTestingModalClose}
				/>
			</React.Fragment>
		);
	}
}

function validate(values)
{
	const errors = {};

	values = _.mapValues(values, Number);

	if (values.lambda <= values.mu) {
		errors.lambda = '\u03bb должно быть больше \u03bc';
		errors.mu = '\u03bc должно быть меньше \u03bb';
	}

	if (values.mu < 1 || values.mu % 2 !== 0)
		errors.mu = '\u03bc должно быть четным положительным числом';

	if (values.lambda < 1 || values.lambda % 2 !== 0)
		errors.lambda = '\u03bb должно быть четным положительным числом';

	if (values.rounds < 1)
		errors.rounds = 'Количество поколений должно быть положительным числом';

	if (values.rounds % 1 !== 0)
		errors.rounds = 'Количество поколений должно быть целым числом';

	if (values.samples < 4)
		errors.samples = 'Количество отсчетов не может быть меньше 4';

	if (values.samples > 48 * 1024 / 4)
		errors.samples = `Количество отсчетов не может превышать ${48 * 1024 / 4}`;

	if (values.samples % 1 !== 0)
		errors.samples = 'Количество отсчетов должно быть целым числом';

	return errors;
}

module.exports = Training;
