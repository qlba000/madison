const _ = require('lodash');
const React = require('react');
const Select = require('../Select');
const {
	Col, CustomInput, FormGroup, InputGroup, InputGroupAddon, InputGroupText, Label, Popover, Row
} = require('reactstrap');
const PropTypes = require('prop-types');
const {models, norms, implications, defuzzifications} = require('./approach');


// function OptionWithPopover(props)
// {
// 	return (
// 		<Popover
//
// 		/>
// 	);
// }


function Option({values, onChange, name, options, questionable, learnable, caption})
{
	const select = (
		<Select
			name={name}
			options={options}
			onChange={(value) => onChange({...values, [name]: {...values[name], value}})}
			value={values[name].value}
		/>
	);

	return (
		<Col xl={4} lg={6}>
			<FormGroup>
				<Label for={`${name}Select`}>
					{!questionable ? caption : (
						<span className="questionable" tabIndex="1234">{caption}</span>
					)}
				</Label>
				{!learnable ? select : (
					<InputGroup>
						{select}
						<InputGroupAddon addonType="append">
							<InputGroupText className="bg-light">
								<CustomInput
									type="checkbox"
									id={`${name}SelectIsLearned`}
									name={`${name}SelectIsLearned`}
									label={<abbr title="Может ли параметр изменяться в процессе настройки (обучения)">Об.</abbr>}
									checked={values[name].learn}
									onChange={() => onChange({...values, [name]: {...values[name], learn: !values[name].learn}})}
								/>
							</InputGroupText>
						</InputGroupAddon>
					</InputGroup>
				)}
			</FormGroup>
		</Col>
	);
}

function Options(props)
{
	const common = _.pick(props, 'values', 'onChange'), isLogical = props.values.model.value === 'logical';

	return (
		<Row>
			<Option key={0} {...common} name="model" options={models} caption="Модель" />
			<Option key={1} {...common} name="indexNorm" options={norms} questionable learnable caption="Норма индексов при свёртке" />
			<Option key={2} {...common} name="valueNorm" options={norms} questionable learnable caption="Норма значений при свёртке" />
			<Option key={3} {...common} name="activNorm" options={norms} questionable learnable caption={`${isLogical ? 'Н' : 'Первая н'}орма при расчёте вывода`} />
			{isLogical ?
				(<Option key={4} {...common} name="activImpl" options={implications} questionable learnable caption="Имплкация при расчёте вывода" />) :
				(<Option key={5} {...common} name="activNorm2" options={norms} questionable learnable caption="Вторая норма при расчёте вывода" />)
			}
			<Option key={6} {...common} name="defMethod" options={defuzzifications} caption="Дефаззификация" />
		</Row>
	);
}

module.exports = Options;
