const _ = require('lodash');
const axios = require('axios').default;
const React = require('react');
const {
	Button,
	Col,
	FormGroup,
	Input,
	Label,
	Row,
	Table,
	UncontrolledTooltip
} = require('reactstrap');
const {AreaChart, Area, XAxis, YAxis} = require('recharts');
const PropTypes = require('prop-types');
const io = require('socket.io-client');
const traverse = require('traverse');
const {tabulateChart} = require('../utils');
const MFEditor = require('../MFEditor');
const {data: funcs} = require('../data');
const {test} = require('./fire');

class DatasetTableElement extends React.Component
{
	static propTypes = {
		value: PropTypes.shape({
			type: PropTypes.string.isRequired
		}),
		variable: PropTypes.shape({
			specs: PropTypes.shape({
				xMin: PropTypes.number.isRequired,
				xMax: PropTypes.number.isRequired
			}).isRequired
		}).isRequired,
		onClick: PropTypes.func.isRequired
	}

	static defaultProps = {
		value: null
	}

	render()
	{
		return (
			<div className="training-set-table-cell clickable" onClick={this.props.onClick}>
				{this.renderValue()}
			</div>
		);
	}

	renderValue()
	{
		const {value} = this.props;

		if (value === null)
			return <span style={{color: 'red', fontWeight: 'bold'}}>Задать...</span>;

		if (value.type === 'delta')
			return value.deltaX.value;

		if (!value.chart) {
			value.chart = tabulateChart(
				funcs[value.type].func,
				value,
				this.props.variable.specs.xMin,
				this.props.variable.specs.xMax,
				true,
				50
			);
		}

		return (
			<AreaChart width={192} height={64} data={value.chart}>
				<XAxis hide type="number" dataKey="x" domain={[this.props.variable.specs.xMin, this.props.variable.specs.xMax]} />
				<YAxis hide type="number" domain={[0, 1]} />
				<Area dataKey="y" stroke="#00f" fill="#00f5" />
			</AreaChart>
		);
	}
}

class OutputElement extends React.Component
{
	static propTypes = {
		// value: PropTypes.number.isRequired,
		onChange: PropTypes.func.isRequired
	}

	constructor(props)
	{
		super(props);

		this.state = this.resetState(props);
	}

	componentWillReceiveProps(props)
	{
		this.setState(this.resetState(props));
	}

	resetState = (props = this.props) => ({value: String(props.value)})

	asFloat = () => Number.parseFloat(this.state.value)

	isValid = () => _.isFinite(this.asFloat())

	onEdit = ({target: {value}}) =>
	{
		this.setState({value});
	}

	onBlur = () =>
	{
		if (this.isValid())
			this.props.onChange(this.asFloat());

		this.setState(this.resetState());
	}

	render()
	{
		return (
			<Input
				style={{margin: '0 1em', width: '10em'}}
				value={this.state.value}
				onChange={this.onEdit}
				onBlur={this.onBlur}
				invalid={!this.isValid()}
			/>
		);
	}
}

class Dataset extends React.Component
{
	static propTypes = {
		// values: PropTypes.shape({
		// 	mu: PropTypes.string.isRequired,
		// 	lambda: PropTypes.string.isRequired,
		// 	rounds: PropTypes.string.isRequired,
		// 	samples: PropTypes.string.isRequired
		// }).isRequired,
		// systemDefinition: PropTypes.any.isRequired,
		// onChange: PropTypes.func.isRequired,
		// onCommit: PropTypes.func.isRequired
	}

	static contextType = require('../NotificationContext')

	state = {
		isMFEditorOpen: false,
		isEditing: null,
		isEvaluating: false,
		computed: {}
	}

	onChange = (key, replacement) =>
	{
		const yndex = key ? _.findIndex(this.props.values, {key}) : Infinity;

		this.props.onChange([
			..._.slice(this.props.values, 0, yndex),
			...replacement ? [replacement] : [],
			..._.slice(this.props.values, yndex + 1)
		]);
	}

	onInputEditClick = (key, input) =>
	{
		this.setState({
			isMFEditorOpen: true,
			isEditing: {key, input}
		});
	}

	onMFSubmit = (value) =>
	{
		const {isEditing: {key, input}} = this.state;
		const oldValue = _.find(this.props.values, {key});

		this.onChange(key, {
			...oldValue,
			inputs: {
				...oldValue.inputs,
				[input]: value
			}
		});
	}

	onMFClose = () =>
	{
		this.setState({
			isMFEditorOpen: false,
			isEditing: null
		});
	}

	onEvaluateClick = () =>
	{
		const systemDefinition = traverse(this.props.systemDefinition).map(function ()
		{
			if (this.node.learn === true)
				this.node.learn = false;
		});

		const caseSet = _(this.props.values).clone();

		_(caseSet).sortBy('key');

		this.setState({isEvaluating: true});

		test(
			{...systemDefinition, trainingSet: caseSet},
			[],
			systemDefinition.training.samplesCount
		).then((results) => {
			this.setState({computed: _.zipObject(_.map(caseSet, 'key'), results.values)});
		}).catch((err) => {
			this.context('danger', 'Вычисление', `Ошибка при вычислении: ${err.message}`);
		}).then(() => this.setState({isEvaluating: false}));
	}

	render()
	{
		if (!this.props.outputVariables[0]) return (
			<div
				style={{
					height: '100%',
					display: 'flex',
					flexDirection: 'column',
					justifyContent: 'space-around',
					alignSelf: 'center'
				}}
			>
				<p style={{fontSize: '32px', color: 'grey', textAlign: 'center'}}>Нет выходной переменной</p>
			</div>
		);

		return (
			<React.Fragment>
				<Table
					bordered
					size="sm"
					className="mb-0 table-dense"
					style={{
						display: 'block',
						height: 'calc(500px - 38px - 1rem)',
						width: '100%',
						overflowX: 'scroll',
						overflowY: 'scroll',
						borderLeft: 'none',
						borderRight: 'none'
					}}
				>
					<thead>
						<tr>
							<th />
							{this.props.inputVariables.map(variable => (
								<th key={variable.name}>{variable.name}</th>
							))}
							<th>{this.props.outputVariables[0].name}</th>
							<th />
						</tr>
					</thead>
					<tbody>
						{_.map(this.props.values, (asset, index) => (
							<tr key={asset.key || (asset.key = _.uniqueId())}>
								<th scope="row">{index + 1}</th>
								{this.props.inputVariables.map(variable => (
									<td key={variable.name}>
										<DatasetTableElement
											value={asset.inputs[variable.name]}
											variable={variable}
											onClick={() => this.onInputEditClick(asset.key, variable.name)}
										/>
									</td>
								))}
								<td>
									<div className="training-set-table-cell">
										{this.props.automated ? (
											this.state.computed[asset.key] || 'Не вычислено'
										) : (
											<OutputElement
												value={asset.output}
												onChange={(value) => this.onChange(asset.key, {
													...asset, output: value
												})}
											/>
										)}
									</div>
								</td>
								<td>
									<div
										id={`id${asset.key}-remove`}
										className="training-set-table-cell remove"
										onClick={() => this.onChange(asset.key)}
									>
										&times;
									</div>
									<UncontrolledTooltip
										target={`id${asset.key}-remove`}
										placement="right"
										delay={0}
									>
										Удалить элемент
									</UncontrolledTooltip>
								</td>
							</tr>
						))}
					</tbody>
				</Table>
				<Row>
					<Col xs={this.props.automated ? 6 : 12}>
						<Button
							block
							style={{marginTop: '1rem'}}
							color="primary"
							onClick={() => this.onChange(null, {
								key: _.uniqueId(),
								inputs: _.fromPairs(this.props.inputVariables.map(({name}) => [name, null])),
								output: ''
							})}
						>
							Добавить элемент
						</Button>
					</Col>
					{this.props.automated && (
						<Col xs={6}>
							<Button
								block
								style={{marginTop: '1rem'}}
								color="primary"
								onClick={this.onEvaluateClick}
								disabled={this.state.isEvaluating}
							>
								{this.state.isEvaluating ? 'Вычисление...' : 'Вычислить'}
							</Button>
						</Col>
					)}
				</Row>
				<MFEditor
					isOpen={this.state.isMFEditorOpen}
					onClose={this.onMFClose}
					onSubmit={this.onMFSubmit}
					noLearn
					xMin={this.state.isEditing ? _.find(this.props.inputVariables, {name: this.state.isEditing.input}).specs.xMin : 0}
					xMax={this.state.isEditing ? _.find(this.props.inputVariables, {name: this.state.isEditing.input}).specs.xMax : 1}
					values={this.state.isEditing ? _.find(this.props.values, {key: this.state.isEditing.key}).inputs[this.state.isEditing.input] : null}
				/>
			</React.Fragment>
		);
	}
}

module.exports = Dataset;
