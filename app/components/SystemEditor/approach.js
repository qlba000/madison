exports.models = [
	{value: 'logical', label: 'Логическая'},
	{value: 'mamdani', label: 'Мамдани'}
];

exports.norms = [
	{value: 'maxmin', label: 'Максминная'},
	{value: 'probabilistic', label: 'Вероятностная'},
	{value: 'lukasiewicz', label: 'Лукасевича'},
	{value: 'strong', label: 'Сильная'},
	{value: 'nullpotent', label: 'Нильпотентная'}
];

exports.implications = [
	{value: 'standard', label: 'Стандартная'},
	{value: 'arithmetical', label: 'Арифметическая'},
	{value: 'lukasiewicz', label: 'Лукасевича'},
	{value: 'aliev1', label: 'Алиева 1'},
	{value: 'aliev2', label: 'Алиева 2'},
	{value: 'aliev4', label: 'Алиева 4'},
	{value: 'godel', label: 'Гёделя'},
	{value: 'richebach', label: 'Райхебаха'},
	{value: 'zadeh', label: 'Заде'},
	{value: 'recher', label: 'Рёшера'},
	{value: 'clincidenis', label: 'Клине-Дениса'},
	{value: 'hoguen', label: 'Гогуэна'},
	{value: 'fouett', label: 'Фоуэтта'},
	{value: 'vadhi', label: 'Вади'}
];

exports.defuzzifications = [
	{value: 'cog', label: 'Центр тяжести'},
	{value: 'coa', label: 'Центр площади'}
];
