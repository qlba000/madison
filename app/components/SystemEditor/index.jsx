const _ = require('lodash');
const axios = require('axios').default;
const React = require('react');
const io = require('socket.io-client');
const {
	Button,
	Col,
	ListGroup,
	ListGroupItem,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Row
} = require('reactstrap');
const PropTypes = require('prop-types');
const cleaner = require('deep-cleaner');
const NotificationContext = require('../NotificationContext');
const Options = require('./Options');
const Variables = require('./Variables');
const Rules = require('./Rules');
const Dataset = require('./Dataset');
const Training = require('./Training');

class SystemEditor extends React.Component
{
	static propTypes = {
		values: PropTypes.any.isRequired,
		isOpen: PropTypes.bool.isRequired,
		onDrop: PropTypes.func.isRequired,
		onCopy: PropTypes.func.isRequired,
		onExit: PropTypes.func.isRequired,
		onSave: PropTypes.func.isRequired,
		name: PropTypes.string
	}

	static defaultProps = {
		name: ''
	}

	static contextType = NotificationContext

	state = {menuItem: null, edited: false, saving: false}

	componentWillReceiveProps = (newProps) =>
	{
		if (this.props.isOpen || !newProps.isOpen)
			return;

		this.setState({
			menuItem: 'Конфигурация',
			values: newProps.values,
			edited: false,
			saving: false
		});
	}

	onChange = newValues =>
	{
		this.setState({
			values: {...this.state.values, ...newValues},
			edited: true
		});
	}

	onCommit = paramsValues =>
	{
		const values = cleaner(_.cloneDeep(this.state.values), ['table', 'chart', 'key']);

		for (const paramName in paramsValues)
		{
			const paramValue = paramsValues[paramName];
			const bits = paramName.split('.');

			if (bits[0] === 'rules')
				values.rules[Number(bits[1])].weight.value = paramValue;
			else
				_.find(
					_.find(
						values[bits[0]],
						{name: bits[1]}
					).specs.terms,
					{name: bits[2]}
				).mf[bits[3]].value = paramValue;
		}

		this.setState({values, edited: true});
	}

	onSubmit = () =>
	{
		const values = cleaner(_.cloneDeep(this.state.values), ['table', 'chart', 'key']);

		this.setState({saving: true});

		return this.props.onSave(values)
			.then(() => this.setState({saving: false, edited: false}));
	}

	onCopy = () =>
	{
		this.setState({saving: true});

		this.props.onCopy(cleaner(_.cloneDeep(this.state.values), ['table', 'chart', 'key']))
			.then(this.props.onExit);
	}

	onDrop = () =>
	{
		this.setState({saving: true});
		this.props.onDrop();
	}

	render()
	{
		return (
			<React.Fragment>
				<Modal
					id="systemeditor-modal"
					backdrop="static"
					isOpen={this.props.isOpen}
					centered
					className="wide-modal"
				>
					<ModalHeader>
						{this.props.name}
					</ModalHeader>
					<ModalBody>
						{this.renderBody()}
					</ModalBody>
					<ModalFooter>
						<Button
							color="danger"
							onClick={this.onDrop}
							disabled={this.state.saving}
						>
							Удалить систему
						</Button>
						<Button
							color={this.state.edited ? 'primary' : 'secondary'}
							onClick={this.onCopy}
							disabled={this.state.saving}
						>
							Сохранить как копию
						</Button>
						<Button
							color={this.state.edited ? 'secondary' : 'primary'}
							onClick={this.props.onExit}
						>
							Выход
						</Button>
						<Button
							color={this.state.edited ? 'primary' : 'secondary'}
							onClick={this.onSubmit}
							disabled={this.state.saving}
						>
							Сохранить изменения
						</Button>
					</ModalFooter>
				</Modal>
			</React.Fragment>
		);
	}

	renderBody()
	{
		return (
			<Row style={{minHeight: '500px'}}>
				<Col md={3} className="border-right mb-4">
					{this.renderMenu()}
				</Col>
				<Col md={9}>
					{this.renderMenuItem()}
				</Col>
			</Row>
		);
	}

	renderMenu()
	{
		return (
			<ListGroup>
				{[
					'Конфигурация',
					'Входные переменные',
					'Выходные переменные',
					'Правила',
					'Обучающая выборка',
					'Тестовая выборка',
					'Обучение',
					'Вывод'
				].map(name => (
					<ListGroupItem
						action
						active={this.state.menuItem === name}
						onClick={() => this.setState({menuItem: name})}
					>
						{name}
					</ListGroupItem>
				))}
			</ListGroup>
		);
	}

	renderMenuItem()
	{
		const {values} = this.state;

		if (!values) return null;

		switch (this.state.menuItem)
		{
		case 'Конфигурация':
			return (
				<Options
					values={values.options}
					onChange={(options) => this.onChange({options})}
				/>
			);
		case 'Входные переменные':
			return (
				<Variables
					key="input"
					values={values.inputVariables}
					onChange={(inputVariables) => this.onChange({inputVariables})}
				/>
			);
		case 'Выходные переменные':
			return (
				<Variables
					key="output"
					values={values.outputVariables}
					maxCount={1}
					onChange={(outputVariables) => this.onChange({outputVariables})}
				/>
			);
		case 'Правила':
			return (
				<Rules
					inputVariables={values.inputVariables}
					outputVariables={values.outputVariables}
					values={values.rules}
					onChange={(rules) => this.onChange({rules})}
				/>
			);
		case 'Обучающая выборка':
			return (
				<Dataset
					key="training"
					inputVariables={values.inputVariables}
					outputVariables={values.outputVariables}
					values={values.trainingSet}
					onChange={(trainingSet) => this.onChange({trainingSet})}
				/>
			);
		case 'Тестовая выборка':
			return (
				<Dataset
					key="testing"
					inputVariables={values.inputVariables}
					outputVariables={values.outputVariables}
					values={values.testingSet}
					onChange={(testingSet) => this.onChange({testingSet})}
				/>
			);
		case 'Обучение':
			return (
				<Training
					systemDefinition={_.omit(values, 'training')}
					values={values.training}
					onChange={(training) => this.onChange({training})}
					onCommit={this.onCommit}
				/>
			);
		case 'Вывод':
			return (
				<Dataset
					automated
					key="caseset"
					inputVariables={values.inputVariables}
					outputVariables={values.outputVariables}
					values={values.caseSet}
					systemDefinition={values}
					onChange={(caseSet) => this.onChange({caseSet})}
				/>
			);
		default:
			return null;
		}
	}
}

module.exports = SystemEditor;
