const _ = require('lodash');
const React = require('react');
const {
	Button,
	Col,
	FormGroup,
	Input,
	Label,
	ListGroup,
	ListGroupItem,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Row
} = require('reactstrap');
const {Area} = require('recharts');
const PropTypes = require('prop-types');
const cc = require('color-convert');
const {data} = require('../data');
const Tipped = require('../Tipped');
const MFEditor = require('../MFEditor');
const Chart = require('../Chart');
const {tabulateChart} = require('../utils');
const RuleEditor = require('../RuleEditor');

const TippedInput = Tipped('bottom')(Input);

function ruleAntecedentToText(expression)
{
	if (!expression.op)
		return `${expression.variable} @ЕСТЬ@ ${expression.term}`;

	if (expression.op === 'and')
		return expression.args.map(arg =>
		{
			const text = ruleAntecedentToText(arg);

			return arg.op === 'or' ? `(${text})` : text;
		}).join(' @И@ ');

	if (expression.op === 'or')
		return expression.args.map(ruleAntecedentToText).join(' @ИЛИ@ ');
}

const RuleToText = rule => `@ЕСЛИ@ ${ruleAntecedentToText(rule.antecedent)}, @ТО@ ${ruleAntecedentToText(rule.consequent)}`;

function ruleText2React(text)
{
	return text.split('@').map((lex, idx) => (
		<span key={idx} className={idx % 2 ? 'keywird' : ''}>{lex}</span>
	));
}

class Rules extends React.Component
{
	static propTypes = {
		inputVariables: PropTypes.any.isRequired,
		outputVariables: PropTypes.any.isRequired,
		values: PropTypes.any.isRequired,
		onChange: PropTypes.func.isRequired
	}

	constructor(props)
	{
		super(props);

		this.state = {
			index: null
		};
	}

	onRuleEditorSubmit = (value) =>
	{
		const {index} = this.state;

		this.props.onChange([
			...this.props.values.slice(0, index),
			...value ? [value] : [],
			...this.props.values.slice(index + 1)
		]);
	}

	render()
	{
		if (!this.props.outputVariables[0]) return (
			<div
				style={{
					height: '100%',
					display: 'flex',
					flexDirection: 'column',
					justifyContent: 'space-around',
					alignSelf: 'center'
				}}
			>
				<p style={{fontSize: '32px', color: 'grey', textAlign: 'center'}}>Нет выходной переменной</p>
			</div>
		);

		return (
			<div
				style={{
					minHeight: '100%',
					display: 'flex',
					flexDirection: 'column',
					justifyContent: 'space-between'
				}}
			>
				<div style={{flexGrow: 1}}>
					{!_.isEmpty(this.props.values) ? (
						<div style={{height: `${500 - 38 - 15}px`, overflowY: 'scroll'}}>
							{this.props.values.map((rule, index) => (
								<div className="ruleview" onClick={() => this.setState({index})}>
									<div className="d-block d-xl-flex">
										<div>{ruleText2React(RuleToText(rule))}</div>
										<div style={{color: 'grey', paddingLeft: '0.25rem'}}>
											<span className="d-inline d-xl-none">Вес: </span>
											{rule.weight.value}
										</div>
									</div>
								</div>
							))}
						</div>
					) : (
						<div
							style={{
								height: '100%',
								display: 'flex',
								flexDirection: 'column',
								justifyContent: 'space-around',
								alignSelf: 'center'
							}}
						>
							<p style={{fontSize: '2em', color: 'grey', textAlign: 'center'}}>Нет правил</p>
						</div>
					)}
				</div>
				<Button
					block
					color="primary"
					onClick={() => this.setState({index: this.props.values.length})}
				>
					Создать правило
				</Button>
				<RuleEditor
					inputVariables={this.props.inputVariables}
					outputVariables={this.props.outputVariables}
					values={this.props.values[this.state.index] || null}
					submit={this.onRuleEditorSubmit}
					isOpen={this.state.index !== null}
					onClose={() => this.setState({index: null})}
				/>
			</div>
		);
	}
}

module.exports = Rules;
