exports.tabulateChart = (func, values, xMin, xMax, isValid, N = 250) =>
{
	const chartPoints = [];

	if (isValid)
		for (let i = 0; i <= N; i++)
		{
			const min = xMin + (i - 0.5) * (xMax - xMin) / N;
			const max = xMin + (i + 0.5) * (xMax - xMin) / N;

			chartPoints.push({x: (min + max) / 2, y: func(values, min, max)});
		}

	return chartPoints;
};
