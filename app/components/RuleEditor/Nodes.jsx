const React = require('react');
const {UncontrolledTooltip} = require('reactstrap');
const _ = require('lodash');

const Envelope = ({classNames = [], children}) => (
	<div className={['antecedent-block', ...classNames].join(' ')}>
		{children}
	</div>
);

const UniqueIdHOC = Component => class extends React.Component
{
	constructor(props)
	{
		super(props);

		this.id = `id${_.uniqueId()}`;
	}

	render()
	{
		return <Component id={this.id} {...this.props} />;
	}
};

const TooltippedButton = UniqueIdHOC(({tooltip, children, id, ...props}) => (
	<React.Fragment>
		<div key="ki" id={id} {...props}>
			{children}
		</div>
		{tooltip && (
			<UncontrolledTooltip key="kiy" placement="bottom" target={id} delay={0}>
				{tooltip}
			</UncontrolledTooltip>
		)}
	</React.Fragment>

));

const AppendButton = ({onClick, label, tooltip}) => (
	<TooltippedButton tooltip={tooltip} className="append" onClick={onClick}>
		{label}
	</TooltippedButton>
);

const RemoveButton = ({onClick}) => (
	<TooltippedButton tooltip="Удалить подусловие" className="remove" onClick={onClick}>
		&times;
	</TooltippedButton>
);


const OriginNode = ({onAppend, label = 'СОЗДАТЬ УСЛОВИЕ'}) => (
	<Envelope classNames={['invalid']}>
		<AppendButton onClick={onAppend} label={label} tooltip={false} />
	</Envelope>
);

const SimpleNode = ({variable, term, onAppend, onRemove}) => (
	<Envelope>
		<TooltippedButton tooltip="Заменить" className="replace" onClick={onAppend}>
			{variable}
			<span className="keywird">ЕСТЬ</span>
			{term}
		</TooltippedButton>
		<RemoveButton onClick={onRemove} />
	</Envelope>
);

const operationToInfix = {and: 'И', or: 'ИЛИ'};

const ComplexNode = ({operation, subconditions, onAppend, onRemove}) =>
{
	const infix = operationToInfix[operation];
	const nodes = _(subconditions)
		.map(subcondition => [subcondition, <span className="keywird">{infix}</span>])
		.flatten()
		.value();

	return (
		<Envelope classNames={_.isEmpty(subconditions) ? ['invalid'] : []}>
			{_.isEmpty(subconditions) || <div>{_.initial(nodes)}</div>}
			<AppendButton
				onClick={onAppend}
				tooltip={_.isEmpty(subconditions) ? false : 'Добавить операнд'}
				label={_.isEmpty(subconditions) ? 'ДОБАВИТЬ ОПЕРАНД' : `+ ${infix} ...`}
			/>
			<RemoveButton onClick={onRemove} />
		</Envelope>
	);
};

module.exports = {OriginNode, SimpleNode, ComplexNode};
