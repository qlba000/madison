const React = require('react');
const {
	Button,
	Col,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Row
} = require('reactstrap');
const PropTypes = require('prop-types');


class Selector extends React.Component
{
	static propTypes = {
		allowOperations: PropTypes.bool.isRequired,
		variables: PropTypes.any.isRequired,
		isOpen: PropTypes.bool.isRequired,
		submit: PropTypes.func.isRequired,
		onClose: PropTypes.func.isRequired
	}

	submit = value =>
	{
		this.props.submit(value);
		this.props.onClose();
	}

	render()
	{
		return (
			<React.Fragment>
				<Modal
					id="ruleeditor-selector-modal"
					backdrop="static"
					isOpen={this.props.isOpen}
					// toggle={this.props.onClose}
					centered
					className="wide-modal"
				>
					<ModalHeader>
						Подусловие
					</ModalHeader>
					<ModalBody>
						{this.renderBody()}
					</ModalBody>
					<ModalFooter>
						<Button
							color="secondary"
							onClick={this.props.onClose}
						>
							Отмена
						</Button>
					</ModalFooter>
				</Modal>
			</React.Fragment>
		);
	}

	renderBody()
	{
		return (
			<Row className="mb-4 pl-4 pr-4">
				{this.props.allowOperations && (
					<Col xl={2} lg={3} md={4} sm={6}>
						<h5 className="mt-4" style={{textAlign: 'center'}}>Выражение</h5>
						<Button
							block
							color="info"
							className="mt-4"
							onClick={() => this.submit({op: 'and', args: []})}
						>
							Конъюнкт
							<br />
							(... И ...)
						</Button>
						<Button
							block
							color="info"
							className="mt-4"
							onClick={() => this.submit({op: 'or', args: []})}
						>
							Дизъюнкт
							<br />
							(... ИЛИ ...)
						</Button>
					</Col>
				)}
				{this.props.variables.map(variable => (
					<Col xl={2} lg={3} md={4} sm={6}>
						<h5 className="mt-4" style={{textAlign: 'center'}}>{variable.name}</h5>
						{variable.specs.terms.map(term => (
							<Button
								block
								color="primary"
								className="mt-4"
								onClick={() => this.submit({variable: variable.name, term: term.name})}
							>
								{variable.name} ЕСТЬ &quot;{term.name}&quot;
							</Button>
						))}
					</Col>
				))}
			</Row>
		);
	}
}

module.exports = Selector;
