const React = require('react');
const _ = require('lodash');
const PropTypes = require('prop-types');
// const {UncontrolledTooltip} = require('reactstrap');
const {OriginNode, SimpleNode, ComplexNode} = require('./Nodes');

class AntecedentEditor extends React.Component
{
	static propTypes = {
		showSelector: PropTypes.func.isRequired,
		values: PropTypes.any.isRequired,
		update: PropTypes.func.isRequired
	}

	update = values => this.props.update(values);

	render()
	{
		return this.renderSubcondition(this.props.values, []);
	}

	renderSubcondition(subcondition, path)
	{
		const showSelector = x => () => this.props.showSelector(x);
		const removal = () => this.update(_.isEmpty(path) ? null : spliceObject(this.props.values, path));

		if (subcondition === null)
			return <OriginNode onAppend={showSelector(this.update)} />;

		if (subcondition.op === undefined)
			return (
				<SimpleNode
					onAppend={showSelector(nv => this.update(_.isEmpty(path) ? nv : updateObject(this.props.values, path, nv)))}
					onRemove={removal}
					variable={subcondition.variable}
					term={subcondition.term}
				/>
			);

		if (_.includes(['and', 'or'], subcondition.op))
		{
			return (
				<ComplexNode
					operation={subcondition.op}
					subconditions={subcondition.args.map((sc, idx) => this.renderSubcondition(sc, [...path, 'args', idx]))}
					onAppend={showSelector(nv => this.update(appendObject(this.props.values, [...path, 'args'], nv)))}
					onRemove={removal}
				/>
			);
		}

		throw new Error('Invalid node type');
	}
}

module.exports = AntecedentEditor;


function updateObject(object, path, replacement)
{
	return _.set(_.cloneDeep(object), path, replacement);
}

function spliceObject(object, path)
{
	_.get(object, _.initial(path)).splice(_.last(path), 1);

	return _.cloneDeep(object);
}

function appendObject(object, path, appendix)
{
	_.get(object, path).push(appendix);

	return _.cloneDeep(object);
}
