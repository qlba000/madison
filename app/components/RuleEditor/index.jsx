const _ = require('lodash');
const React = require('react');
const {
	Button,
	Col,
	FormGroup,
	Input,
	Label,
	ListGroup,
	ListGroupItem,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Row
} = require('reactstrap');
const {Area} = require('recharts');
const PropTypes = require('prop-types');
const cc = require('color-convert');
const {data} = require('../data');
const Tipped = require('../Tipped');
const MFEditor = require('../MFEditor');
const Chart = require('../Chart');
const {tabulateChart} = require('../utils');
const AntecedentEditor = require('./AntecedentEditor');
const Selector = require('./Selector');
const {OriginNode, SimpleNode} = require('./Nodes');
const ParamInput = require('../ParamInput.jsx');

const TippedInput = Tipped('bottom')(Input);

function isFloat(string)
{
	return Number.isFinite(Number.parseFloat(string));
}

function validate(weight)
{
	if (weight === '')
		return 'Введите вес правила';

	if (!isFloat(weight))
		return 'Вес должен быть числом';

	if (weight < 0)
		return 'Вес не может быть отрицательным';

	if (weight > 1)
		return 'Вес не может быть больше единицы';
}

function isAntecedentValid(antecedent)
{
	if (antecedent === null)
		return false;

	if (antecedent.args)
	{
		if (_.isEmpty(antecedent.args))
			return false;

		return antecedent.args.every(isAntecedentValid);
	}

	return true;
}

class RuleEditor extends React.Component
{
	static propTypes = {
		inputVariables: PropTypes.any.isRequired,
		outputVariables: PropTypes.any.isRequired,
		values: PropTypes.any.isRequired,
		submit: PropTypes.func.isRequired,
		isOpen: PropTypes.bool.isRequired,
		onClose: PropTypes.func.isRequired
	}

	constructor(props)
	{
		super(props);

		this.state = this.getInitialState(props);
	}

	componentWillReceiveProps(newProps)
	{
		this.setState(this.getInitialState(newProps));
	}

	getInitialState = (props) => ({
		values: _.cloneDeep(props.values || {
			antecedent: null,
			consequent: null,
			weight: {
				value: '1',
				learn: false
			}
		}),
		selectorAllowOperations: false,
		selectorIsOpen: false,
		selectorSubmit: null,
		selectorVariables: []
	});

	showSelector = (variables, allowOperations) => submit =>
	{
		this.setState({
			selectorAllowOperations: allowOperations,
			selectorVariables: variables,
			selectorSubmit: submit,
			selectorIsOpen: true
		});
	}

	submit = value =>
	{
		value.weight.value = Number(value.weight.value);

		this.props.submit(value);
		this.props.onClose();
	}

	render()
	{
		const {antecedent, consequent} = this.state.values;

		return (
			<React.Fragment>
				<Modal
					id="ruleeditor-modal"
					backdrop="static"
					isOpen={this.props.isOpen}
					// toggle={this.props.onClose}
					centered
					className="wide-modal"
				>
					<ModalHeader>
						Правило
					</ModalHeader>
					<ModalBody>
						{this.renderBody()}
					</ModalBody>
					<ModalFooter>
						{this.props.values && (
							<Button
								color="danger"
								onClick={() => this.submit(null)}
							>
								Удалить
							</Button>
						)}
						<Button
							color="secondary"
							onClick={this.props.onClose}
						>
							Отмена
						</Button>
						<Button
							color="primary"
							onClick={() => this.submit(this.state.values)}
							disabled={!isAntecedentValid(antecedent) || !consequent}
						>
							ОК
						</Button>
					</ModalFooter>
				</Modal>
				<Selector
					allowOperations={this.state.selectorAllowOperations}
					variables={this.state.selectorVariables}
					isOpen={this.state.selectorIsOpen}
					submit={this.state.selectorSubmit}
					onClose={() => this.setState({selectorIsOpen: false})}
				/>
			</React.Fragment>
		);
	}

	renderBody()
	{
		return (
			<React.Fragment>
				<h5>Антецедент</h5>
				<hr />
				{this.renderAntecedent()}
				<h5 className="mt-5">Консеквент</h5>
				<hr />
				{this.renderConsequent()}
				<h5 className="mt-5">Вес</h5>
				<hr />
				{this.renderWeight()}
			</React.Fragment>
		);
	}

	renderAntecedent()
	{
		return (
			<AntecedentEditor
				showSelector={this.showSelector(this.props.inputVariables, true)}
				values={this.state.values.antecedent}
				update={antecedent => this.setState({values: {...this.state.values, antecedent}})}
			/>
		);
	}

	renderConsequent = () =>
	{
		const onChange = () => this.showSelector(
			this.props.outputVariables, false
		)(
			consequent => this.setState({values: {...this.state.values, consequent}})
		);

		const {consequent} = this.state.values;

		return consequent ? (
			<SimpleNode
				variable={consequent.variable}
				term={consequent.term}
				onAppend={onChange}
				onRemove={() => this.setState({values: {...this.state.values, consequent: null}})}
			/>
		) : (
			<OriginNode
				onAppend={onChange}
				label="СОЗДАТЬ ЗАКЛЮЧЕНИЕ"
			/>
		);
	}

	renderWeight()
	{
		const {weight} = this.state.values;

		return (
			<Row>
				<Col xs={3}>
					<ParamInput
						name="weight"
						label="Вес"
						values={{weight}}
						errors={{weight: {value: validate(weight.value)}}}
						setFieldValue={(name, weight) => this.setState({values: {...this.state.values, weight}})}
					/>
				</Col>
			</Row>
		);
	}
}

module.exports = RuleEditor;
