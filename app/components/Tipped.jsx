const React = require('react');
const {UncontrolledTooltip} = require('reactstrap');
const PropTypes = require('prop-types');

module.exports = (placement = 'right') => Input =>
{
	const Component = (props) => (
		<React.Fragment>
			<Input
				key={0}
				invalid={Boolean(props.error)}
				{...props}
			/>
			{props.error && (
				<UncontrolledTooltip
					key={1}
					placement={placement}
					target={props.id}
				>
					{props.error}
				</UncontrolledTooltip>
			)}
		</React.Fragment>
	);

	Component.propTypes = {
		error: PropTypes.object.isRequired,
		id: PropTypes.string.isRequired
	};

	return Component;
};
