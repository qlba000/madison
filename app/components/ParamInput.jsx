const React = require('react');
const {
	FormGroup,
	Tooltip,
	Label,
	Col,
	CustomInput,
	InputGroup,
	InputGroupAddon,
	InputGroupText
} = require('reactstrap');
const PropTypes = require('prop-types');
const NumberInput = require('./NumberInput.jsx');

function ParamInput({name, label, noLearn, values, errors, setFieldValue})
{
	const setValue = fields => setFieldValue(name, {...values[name], ...fields});
	const error = errors[name] && errors[name].value;

	const input = (
		<NumberInput
			name={name}
			id={name}
			value={values[name].value}
			onChange={e => setValue({value: e.target.value})}
			invalid={Boolean(error)}
		/>
	);

	return (
		<FormGroup row>
			<Label htmlFor={name} sm="2">
				{label}
			</Label>
			<Col sm="10">
				{noLearn ? input : (
					<InputGroup id={`${name}InputGroup`}>
						{input}
						<InputGroupAddon addonType="append">
							<InputGroupText className="bg-light">
								<CustomInput
									type="checkbox"
									id={`${name}IsLearned`}
									name={`${name}IsLearned`}
									label={<abbr title="Может ли параметр изменяться в процессе настройки (обучения)">Об.</abbr>}
									checked={values[name].learn}
									onChange={() => setValue({learn: !values[name].learn})}
								/>
							</InputGroupText>
						</InputGroupAddon>
					</InputGroup>
				)}
				{error && (
					<Tooltip
						placement="right"
						target={`${name}${noLearn ? '' : 'InputGroup'}`}
						isOpen={Boolean(error)}
					>
						{error}
					</Tooltip>
				)}
			</Col>
		</FormGroup>
	);
}

ParamInput.propTypes = {
	name: PropTypes.string.isRequired,
	label: PropTypes.string.isRequired,
	noLearn: PropTypes.bool,
	values: PropTypes.string.isRequired,
	errors: PropTypes.object.isRequired,
	setFieldValue: PropTypes.func.isRequired
};

ParamInput.defaultProps = {
	noLearn: false
};

module.exports = ParamInput;
