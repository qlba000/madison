const React = require('react');
const RegexInput = require('./RegexInput.jsx');

function NumberInput(props)
{
	return (
		<RegexInput
			{...props}
			regex={/^[+-]?(\d*\.?\d*|(\d+\.?\d*|\d*\.?\d+)([eE][+-]?\d*)?)$/}
		/>
	);
}

module.exports = NumberInput;
