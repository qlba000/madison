const React = require('react');
const styled = require('styled-components').default;
const {
	ResponsiveContainer,
	AreaChart,
	CartesianGrid,
	XAxis,
	YAxis,
	Tooltip
} = require('recharts');
const PropTypes = require('prop-types');
const sprintf = require('printf');

function Chart({children, xMin, xMax})
{
	return (
		<ResponsiveContainer width="100%" aspect={2}>
			<AreaChart>
				<CartesianGrid strokeDasharray="3 3" />
				<XAxis type="number" domain={[xMin, xMax]} dataKey="x" />
				<YAxis type="number" domain={[0, 1]} allowDataOverflow />
				{/* <Tooltip
					content={CustomTooltip}
					cursor={{strokeDasharray: '3 3'}}
				/> */}
				{children}
			</AreaChart>
		</ResponsiveContainer>
	);
}

const TooltipShape = styled.div`
	background-color: #000b;
	color: #fff;

	min-width: 7em;
	border-radius: 5px;
	padding: 7px;
`;

function CustomTooltip({active, payload})
{
	if (active && payload)
	{
		return (
			<TooltipShape>
				x: {sprintf('%.4g', payload[0].payload.x)}
				<br />
				{payload.map(({name, payload: {y}}, index) => (
					<React.Fragment>
						{index > 0 && (<br />)}
						{name}: {sprintf('%.4g', y)}
					</React.Fragment>
				))}
			</TooltipShape>
		);
	}
}


Chart.propTypes = {
	children: PropTypes.node.isRequired,
	xMin: PropTypes.number.isRequired,
	xMax: PropTypes.number.isRequired
};

module.exports = Chart;
