const _ = require('lodash');
const React = require('react');
const PropTypes = require('prop-types');
const {
	Alert,
	Button,
	Col,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Row,
	UncontrolledTooltip
} = require('reactstrap');
const {
	ResponsiveContainer,
	CartesianGrid,
	XAxis,
	YAxis,
	ZAxis,
	Tooltip,
	ScatterChart,
	Scatter
} = require('recharts');

const WING_SIZE = 5;

const Line = ({x1, y1, x2, y2}) => (
	<line
		stroke="#000"
		strokeWidth="1px"
		strokeOpacity={1}
		fill="none"
		x1={x1}
		y1={y1}
		x2={x2}
		y2={y2}
	/>
);

const Cross = ({cx: x, cy: y}) => (
	<React.Fragment>
		<Line x1={x - WING_SIZE} y1={y - WING_SIZE} x2={x + WING_SIZE} y2={y + WING_SIZE} />
		<Line x1={x - WING_SIZE} y1={y + WING_SIZE} x2={x + WING_SIZE} y2={y - WING_SIZE} />
	</React.Fragment>
);


class TestingModal extends React.Component
{
	static propTypes = {
		error: PropTypes.string.isRequired,
		chart: PropTypes.arrayOf(PropTypes.shape({
			round: PropTypes.number.isRequired,
			best: PropTypes.number.isRequired,
			aver: PropTypes.number.isRequired,
			disp: PropTypes.number.isRequired
		}).isRequired),
		chartRange: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
		fitness: PropTypes.number,
		isOpen: PropTypes.bool.isRequired,
		onClose: PropTypes.func.isRequired
	}

	static defaultProps = {
		chart: [],
		fitness: null
	}

	render()
	{
		return (
			<Modal
				id="testing-modal"
				backdrop="static"
				isOpen={this.props.isOpen}
				centered
				className="wide-modal"
			>
				<ModalHeader>
					Разброс на тестовой выборке
				</ModalHeader>
				<ModalBody>
					{this.renderBody()}
				</ModalBody>
				<ModalFooter>
					{this.renderAlert()}
					<Button
						color="primary"
						onClick={this.props.onClose}
					>
						Выход
					</Button>
				</ModalFooter>
			</Modal>
		);
	}

	renderBody()
	{
		return (
			<Row>
				<Col xl={{size: 6, offset: 3}} lg={{size: 8, offset: 2}} md={{size: 10, offset: 1}}>
					<ResponsiveContainer width="100%" aspect={1}>
						<ScatterChart
							// margin={{top: 5, right: 30, left: 20, bottom: 5}}
							margin={0}
						>
							<CartesianGrid strokeDasharray="3 3" />
							<XAxis name="Ожидаемое значение" type="number" dataKey="expected" domain={this.props.chartRange} />
							<YAxis name="Фактическое значение" type="number" dataKey="received" domain={this.props.chartRange} />
							<ZAxis name="-" type="number" dataKey="-" domain={[0, 1]} range={[500, 500]} />
							<Tooltip />
							<Scatter
								shape={Cross}
								data={this.props.chart}
								fill="#000"
							/>
							<Scatter
								shape={() => null}
								line
								fill="#666"
								data={[{
									expected: this.props.chartRange[0],
									received: this.props.chartRange[0]
								}, {
									expected: this.props.chartRange[1],
									received: this.props.chartRange[1]
								}]}
							/>
						</ScatterChart>
					</ResponsiveContainer>
					<div
						style={{
							position: 'absolute',
							top: '1rem',
							margin: 'auto',
							width: '100%',
							textAlign: 'center',
							color: '#666',
							fontSize: '2rem'
						}}
					>
						{this.props.fitness && `R = ${this.props.fitness}`}
					</div>
				</Col>
			</Row>
		);
	}

	renderAlert()
	{
		return (
			<Alert
				color={
					this.props.error ? 'danger' :
						this.props.fitness !== null ? 'success' :
							'primary'
				}
				style={{
					marginBottom: 0,
					padding: '6px 12px',
					width: '100%'
				}}
			>
				{this.props.error || (this.props.fitness !== null ?
					'Тестирование успешно завершено' :
					'Выполняется тестирование...'
				)}
			</Alert>
		);
	}
}

module.exports = TestingModal;
