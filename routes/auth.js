const crypto = require('crypto');
const bcrypt = require('bcryptjs');
const moment = require('moment');
const authed = require('../middleware/auth');
const db = require('../db');

const router = module.exports = require('express-promise-router')();

router.post('/signin', async (req, res) =>
{
	const {username, password} = req.body;

	const user = await db.users.findOne({where: {username}});

	if (!user)
		return res.sendStatus(403);

	if (!await bcrypt.compare(password, user.password))
		return res.sendStatus(403);

	const {count: sessionCount, rows: oldest} = await db.sessions.findAndCountAll({
		where: {userId: user.id},
		order: [['id', 'ASC']],
		limit: 1
	});

	if (sessionCount >= 5)
		await db.sessions.destroy({where: {id: oldest[0].id}});

	const session = await createSession(user);

	res.cookie('session', session.key, {expires: session.expires});
	res.sendStatus(204);
});

router.post('/signup', async (req, res) =>
{
	const {username, password} = req.body;

	if (await db.users.findOne({where: {username}}))
		return res.sendStatus(409);

	const user = await db.users.create({
		username,
		password: await bcrypt.hash(password, 10)
	});

	const session = await createSession(user);

	res.cookie('session', session.key, {expires: session.expires});
	res.sendStatus(204);
});

router.get('/me', authed, (req, res) => res.json(req.user));

function createSession(user)
{
	return db.sessions.create({
		key: crypto.randomBytes(48).toString('base64'),
		expires: moment().add(1, 'year'),
		userId: user.id
	});
}
