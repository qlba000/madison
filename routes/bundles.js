const {resolve} = require('path');

const router = module.exports = require('express-promise-router')();

router.get(/\/frontend.*/, (req, res) =>
{
	res.sendFile(resolve(__dirname, '../static/index.html'));
});
