const router = module.exports = require('express-promise-router')();

router.get('/', (req, res) =>
{
	res.redirect(302, '/frontend');
});
