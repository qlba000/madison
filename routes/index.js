const router = module.exports = require('express-promise-router')();

router.use('/', require('./auth'));
router.use('/', require('./bundles'));
router.use('/', require('./nfisf'));
router.use('/', require('./root'));
