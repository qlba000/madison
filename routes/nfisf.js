const _ = require('lodash');
const auth = require('../middleware/auth');
const fire = require('../procs/fire');
const db = require('../db');
const ws = require('../ws');

const router = module.exports = require('express-promise-router')();

router.get(
	'/systems',
	auth,
	async (req, res) =>
	{
		const systems = await db.systems.findAll({
			attributes: {exclude: ['definition']},
			where: {userId: req.user.id},
			order: [['id', 'DESC']]
		});

		res.json(systems.map(s => s.toJSON()));
	}
);

router.get(
	'/systems/:id',
	auth,
	async (req, res) =>
	{
		const system = await db.systems.findOne({
			where: {id: req.params.id, userId: req.user.id}
		});

		if (!system)
			throw {...new Error(), status: 404};

		res.json(system.toJSON());
	}
);


router.post(
	'/systems',
	auth,
	async (req, res) =>
	{
		const system = await db.systems.create({
			..._.pick(req.body, 'name', 'definition'),
			userId: req.user.id
		});

		res.json(system.id);
	}
);

router.patch(
	'/systems/:id',
	auth,
	async (req, res) =>
	{
		const [result] = await db.systems.update(
			_.pick(req.body, 'name', 'definition'),
			{where: {id: req.params.id, userId: req.user.id}}
		);

		if (!result)
			throw {...new Error(), status: 404};

		res.send(204);
	}
);

router.delete(
	'/systems/:id',
	auth,
	async (req, res) =>
	{
		const result = await db.systems.destroy(
			{where: {id: req.params.id, userId: req.user.id}}
		);

		if (!result)
			throw {...new Error(), status: 404};

		res.send(204);
	}
);

router.post('/fire', (req, res) =>
{
	const nsp = _.uniqueId('fire');

	ws.server.of(nsp).on('connection', socket => fire(socket, req.body, nsp));

	res.json({nsp});
});
