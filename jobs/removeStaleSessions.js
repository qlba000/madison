const {CronJob} = require('cron');
const db = require('../db');

module.exports = new CronJob('0 */5 * * * *', async function()
{
	const count = await db.sessions.destroy({
		where: {expires: {[db.Op.lt]: new Date()}}
	});

	console.log(`Removed ${count} stale sessions`);
});
