const Sequelize = require('sequelize');

const models = exports.models = {};

models.users = {
	fields: {
		username: {
			type: Sequelize.STRING,
			allowNull: false,
			unique: true
		},
		password: {
			type: Sequelize.STRING,
			allowNull: false
		}
	}
};

models.sessions = {
	fields: {
		key: {
			type: Sequelize.STRING,
			allowNull: false,
			unique: true
		},
		expires: {
			type: Sequelize.DATE,
			allowNull: false
		}
	},
	options: {
		timestamps: false
	}
};

models.systems = {
	fields: {
		name: {
			type: Sequelize.STRING
		},
		definition: {
			type: Sequelize.JSONB,
			allowNull: false
		}
	},
	options: {
		paranoid: true
	}
};

models.requests = {
	fields: {
		time: {
			type: Sequelize.DATE,
			allowNull: false
		},
		method: {
			type: Sequelize.STRING,
			allowNull: false
		},
		url: {
			type: Sequelize.STRING,
			allowNull: false
		},
		params: {
			type: Sequelize.JSONB,
			allowNull: false
		},
		ip: {
			type: Sequelize.INET,
			allowNull: false
		},
		status: {
			type: Sequelize.INTEGER,
			allowNull: true
		}
	},
	options: {
		timestamps: false
	}
};

exports.associate = metalized =>
{
	metalized.sessions.belongsTo(metalized.users, {
		foreignKey: {allowNull: false},
		onDelete: 'CASCADE'
	});

	metalized.systems.belongsTo(metalized.users, {
		foreignKey: {allowNull: false},
		onDelete: 'CASCADE'
	});

	metalized.requests.belongsTo(metalized.users, {
		foreignKey: {allowNull: true},
		onDelete: 'SET NULL'
	});
};
