const Sequelize = require('sequelize');
const {resolve} = require('path');
const {models, associate} = require('./models');
const logger = require('../utils/logger')('sequelize');

exports.init = async () =>
{
	exports.Op = Sequelize.Op;

	const metal = new Sequelize({
		// dialect: 'sqlite',
		// storage: resolve(__dirname, '..', 'db.sqlite')
		database: 'nfisf',
		username: 'nfisf',
		password: 'nfisf',
		host: '127.0.0.1',
		port: 5432,
		dialect: 'postgres',
		pool: {
			min: 0,
			max: 10,
			idle: 1 * 60 * 1000
		},
		logging
	});

	for (const name in models)
		exports[name] = metal.define(name, models[name].fields, {timestamps: true, ...models[name].options});

	associate(exports);

	await metal.authenticate();
	await metal.sync();
};

function logging(message)
{
	logger.debug(
		message
			.replace(/^Executing \(.*?\): /, '')
			.replace(/;$/, '')
			.replace(/\b[A-Z_]+\b/g, match => `{magenta ${match}}`)
			.replace(/`([^`]+)`/g, (match, word) => `{yellow ${word}}`)
			.replace(/'([^']+)'/g, match => `{cyan ${match}}`)
			.replace(/^.*$/, match => `{red ${match}}`)
	);
}
