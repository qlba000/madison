var req = new XMLHttpRequest();

req.addEventListener("progress", function(event)
{
	if (event.lengthComputable)
	{
		var percentComplete = event.loaded / event.total;

		document.getElementById("progress").style.cssText = "width: " + (100 * percentComplete) + "%";
	}
	else
		;
},
false);

// load responseText into a new script element
req.addEventListener("load", function(event)
{
	document.getElementById("progress").style.cssText = "width: 100%";

	var e = event.target;
	var s = document.createElement("script");
	s.innerHTML = e.responseText;

	// or: s[s.innerText!=undefined?"innerText":"textContent"] = e.responseText

	document.documentElement.appendChild(s);

	s.addEventListener("load", function()
	{
		// this runs after the new script has been executed...
	});
},
false);

req.open("GET", "/static/frontend.js");
req.send();
