const chalk = require('chalk');
const printf = require('printf');
const logger = require('../utils/logger')('express');
// const config = require('../config')().logger;

const config = {querystring: false};

module.exports = async (req, res, next) =>
{
	const start = Date.now(), url = req.url, id = req.id = req.app.reqs = req.app.reqs + 1 || 0;

	res.once('finish', () =>
	{
		logger.trace([
			chalk.magenta(req.ip.padEnd(15)),
			chalk.cyan(printf('%4d', id)),
			chalk.gray(printf('%7.3f"', (Date.now() - start) / 1000)),
			mapStatus(res.statusCode),
			mapMethod(req.method),
			chalk[req.urlColor || 'red'](mapUrl(url))
		].join(' '));
	});

	next();
};

function mapStatus(status)
{
	const codes = {
		1: 'cyan',
		2: 'green',
		3: 'magenta',
		4: 'yellow',
		5: 'red'
	};

	return chalk[codes[Math.floor(status / 100)]](status);
}

function mapMethod(method)
{
	const methods = {
		SEARCH: 'blue',
		GET: 'green',
		POST: 'yellow',
		PATCH: 'cyan',
		DELETE: 'red',
		MKCOL: 'magenta'
	};

	return chalk[methods[method] || 'reset'](method.padEnd(6));
}

function mapUrl(url)
{
	if (!config.querystring)
		return url.replace(/\?.*$/, '');

	return url.replace(/([?&])([a-z\d]+)=([a-z\d]+)/gi, (match, prefix, key, value) =>
		`{magenta ${prefix}}{yellow ${key}}{blue =}{cyan ${value}}`);
}
