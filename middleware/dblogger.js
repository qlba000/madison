const db = require('../db');
const auth = require('./auth');

module.exports = async (req, res, next) =>
{
	const time = new Date();
	const url = req.url;

	res.once('finish', async () =>
	{
		const user = req.user || await auth.getUser(req.cookies.session);

		await db.requests.create({
			time,
			method: req.method,
			url,
			params: {
				params: req.params,
				query: req.query,
				body: req.body
			},
			ip: req.ip,
			status: Number(res.statusCode) || 0,
			userId: user && user.id
		});
	});

	next();
};
