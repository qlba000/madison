const {name, version} = require('../package.json');

module.exports = (req, res, next) =>
{
	res.set('X-Powered-By', 'Deadbeef Industries GmbH');

	if (/\.js\.gz$/.test(req.url))
	{
		res.set('Content-Encoding', 'gzip');
		res.set('Content-Type', 'application/json');
	}

	next();
};
