exports.carter = (req, res, next) =>
{
	next({...new Error(), status: 404});
};

exports.error = (err, req, res, next) =>
{
	console.error(err);

	res.status(err.status || 500).send();
};
