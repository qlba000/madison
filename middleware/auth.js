const db = require('../db');

async function getUser(sessionKey)
{
	if (!sessionKey)
		return null;

	const session = await db.sessions.findOne({
		where: {
			key: sessionKey,
			expires: {[db.Op.gte]: new Date()}
		}
	});

	if (!session)
		return null;

	const user = await db.users.findOne({
		where: {id: session.userId}
	});

	if (!user)
		return null;

	return user.toJSON();
}

module.exports = async (req, res, next) =>
{
	const user = await getUser(req.cookies.session);

	if (!user)
		throw {...new Error(), status: 401};

	req.user = user;

	next();
};

module.exports.getUser = getUser;
