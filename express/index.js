const {resolve} = require('path');

const express = require('express');
const cookieParser = require('cookie-parser');

const routes = require('../routes');
const SOLogger = require('../middleware/sologger');
const DBLogger = require('../middleware/dblogger');
const headers = require('../middleware/headers');
const error = require('../middleware/error');

module.exports = express()
	.use(SOLogger)
	.use(headers)
	.use(cookieParser())
	.use(express.json({limit: '100mb'}))
	.use(express.urlencoded({extended: true}))
	.use(DBLogger)
	.use('/static', express.static(resolve(__dirname, '..', 'static')))
	.use('/static', express.static(resolve(__dirname, '..', 'build')))
	.use(routes)
	.use(error.carter)
	.use(error.error);
