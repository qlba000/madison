const {resolve} = require('path');
const zlib = require('zlib');
const fs = require('fs');
const ClosureCompilerPlugin = require('webpack-closure-compiler');
const CompressionPlugin = require('compression-webpack-plugin');

module.exports = {
	// mode: 'development',
	mode: 'production',

	// devtool: 'source-map',

	entry: {
		frontend: './app/index.jsx'
	},

	output: {
		path: resolve(__dirname, 'build')
	},

	module: {
		rules: [
			{
				test: /\.jsx$/,
				use: {
					loader: 'babel-loader',
					options: JSON.parse(fs.readFileSync('./.babelrc').toString())
				}
			},
			{
				test: /\.(scss)$/,
				use: [
					{
						loader: 'style-loader'
					},
					{
						loader: 'css-loader'
					},
					{
						loader: 'postcss-loader',
						options: {
							plugins: () => [
								require('precss'),
								require('autoprefixer')
							]
						}
					},
					{
						loader: 'sass-loader'
					}
				]
			}
		]
	},

	resolve: {
		extensions: ['.js', '.jsx'],
		alias: {
			app: resolve(__dirname, 'app')
		}
	},

	plugins: [
		new ClosureCompilerPlugin({
			compiler: {
				jar: resolve(__dirname, '..', 'compiler.jar'),
//				language_in: 'ECMASCRIPT_2018',
//				language_out: 'ECMASCRIPT5',
//				compilation_level: 'ADVANCED'
			},
			concurrency: 11,
		}),
		new CompressionPlugin({
			filename: '[path].gz[query]',
			algorithm: 'gzip',
			compressionOptions: {level: 9},
			deleteOriginalAssets: true
		})
	]
};
