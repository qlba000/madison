const moment = require('moment');
const printf = require('printf');
const Chalk = require('chalk').constructor;
// const config = require('../config')().logger;

const config = {destinations: [{
	stream: process.stdout,
	colors: 1,
	level: 'verbose'
}]};


const levels = getLevelsHash([
	{name: 'never',   mark: 'NEVER', color: 'white'},
	{name: 'fatal',   mark: 'FATAL', color: 'red'},
	{name: 'error',   mark: 'ERROR', color: 'red'},
	{name: 'warning', mark: 'WARNG', color: 'yellow'},
	{name: 'note',    mark: 'NOTE ', color: 'yellow'},
	{name: 'info',    mark: 'INFO ', color: 'green'},
	{name: 'log',     mark: 'LOG  ', color: 'green'},
	{name: 'trace',   mark: 'TRACE', color: 'cyan'},
	{name: 'debug',   mark: 'DEBUG', color: 'blue'},
	{name: 'verbose', mark: 'VERBS', color: 'magenta'},
	{name: 'silly',   mark: 'SILLY', color: 'gray'},
	{name: 'always',  mark: 'ALWYS', color: 'gray'}
]);


module.exports = messageEmitter =>
{
	const logger = {};

	for (let name in levels)
		logger[name] = (fmt, ...valist) => log(name, messageEmitter, fmt, ...valist);

	log('silly', 'logger', 'created logger for {cyan %s}', messageEmitter);

	return logger;
}

function log(messageLevel, messageEmitter, fmt, ...valist)
{
	const timestamp = moment().format('DD.MMM.YY HH:mm:ss.SSS').toUpperCase();

	for (let {stream, colors, level} of config.destinations)
		if (levels[messageLevel].index <= levels[level].index)
		{
			const chalk = Chalk({level: colors});

			stream.write([
				chalk.gray(`[${timestamp}]`),
				chalk[levels[messageLevel].color](levels[messageLevel].mark),
				chalk.green((`[${messageEmitter}]`).padEnd(12)),
				csprintf(colors, fmt, ...valist)
			].join(' ') + '\n');
		}
}

function csprintf(level, fmt, ...valist)
{
	try
	{
		return Chalk({level})((obj => obj.raw = obj)([printf(fmt, ...valist)]));
	}
	catch (e)
	{
		return `${e.name} while rendering '${[fmt, ...valist].join('\', \'')}': ${e.message}`;
	}
}

function getLevelsHash(levels)
{
	const result = {}, index = 0;

	for (let index in levels)
		result[levels[index].name] = {...levels[index], index: index++};

	return result;
}
