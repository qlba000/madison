module.exports = {
	"extends": "airbnb",
	"parser": "babel-eslint",
	"env": {
		"node": true,
		"browser": true
	},
	"rules": {
		"indent": [
			"error",
			"tab"
		],
		"no-tabs": [
			"off"
		],
		"object-curly-spacing": [
			"error",
			"never"
		],
		"comma-dangle": [
			"error",
			"never"
		],
		"curly": [
			"off"
		],
		"brace-style": [
			"off"
		],
		"object-curly-newline": [
			"off"
		],
		"implicit-arrow-linebreak": [
			"off"
		],
		"nonblock-statement-body-position": [
			"off"
		],
		"func-names": [
			"off"
		],
		"space-before-function-paren": [
			"off"
		],
		"no-restricted-properties": [
			"off"
		],
		"arrow-parens": [
			"off"
		],
		"no-use-before-define": [
			"off"
		],
		"no-plusplus": [
			"off"
		],
		"guard-for-in": [
			"off"
		],
		"no-restricted-syntax": [
			"off"
		],
		"one-var": [
			"off"
		],
		"one-var-declaration-per-line": [
			"off"
		],
		"no-multi-assign": [
			"off"
		],
		"operator-linebreak": [
			"off"
		],
		"consistent-return": [
			"off"
		],
		"react/jsx-indent": [
			"error",
			"tab"
		],
		"react/jsx-one-expression-per-line": [
			"off"
		],
		"react/destructuring-assignment": [
			"off"
		],
		"react/jsx-indent-props": [
			"error",
			"tab"
		],
		"react/forbid-prop-types": [
			"off"
		],
		"jsx-a11y/mouse-events-have-key-events": [
			"off"
		],
		"jsx-a11y/click-events-have-key-events": [
			"off"
		],
		"jsx-a11y/no-static-element-interactions": [
			"off"
		],
		"max-len": [
			"off"
		],
		"no-nested-ternary": [
			"off"
		],
		"react/sort-comp": [
			"off"
		],
		"react/no-access-state-in-setstate": [
			"off"
		],
		"no-param-reassign": [
			"off"
		],
		"prefer-destructuring": [
			"off"
		],
		"react/prefer-stateless-function": [
			"off"
		],
		"no-return-assign": [
			"off"
		],
		"class-methods-use-this": [
			"off"
		],
		"no-case-declarations": [
			"off"
		],
		"react/no-multi-comp": [
			"off"
		],
		"no-throw-literal": [
			"off"
		],
		"prefer-arrow-callback": [
			"off"
		]
	}
};
