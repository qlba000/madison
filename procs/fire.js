const _ = require('lodash');
const {resolve} = require('path');
const {fork} = require('child_process');

const Logger = require('../utils/logger');

const backend = resolve(__dirname, '..', '..', 'madison-backend');

module.exports = async (socket, task, nsp) =>
{
	const logger = Logger(nsp);

	logger.log('forking kernel');

	const kernel = fork(
		resolve(backend, 'index.js'),
		[],
		{env: {MADISON_CORE_PATH: resolve(backend, '.bin', 'a.out')}}
	);

	logger.log(`process ID ${kernel.pid}`);

	let done = false;

	const onError = error =>
	{
		logger.error(error.message);
		socket.once('error', _.noop);
		socket.emit('error', error.message);

		kernel.removeAllListeners();

		kernel.kill();
		socket.disconnect();
	};

	const onMessage = message =>
	{
		if (message.error)
			return onError(message.error);

		// logger.trace(JSON.stringify(message));
		// console.dir(message);

		socket.emit('message', message);

		if (message.result)
			done = true;
	};

	const onExit = (code, signal) =>
	{
		if (!done || code)
			return onError(new Error(`kernel exited with code ${code}, signal ${signal}, done=${done}`));

		logger.log(`kernel exited with code ${code}, signal ${signal}, done=${done}`);

		kernel.removeAllListeners();
		socket.disconnect();
	};

	kernel.on('error', onError);
	kernel.on('message', onMessage);
	kernel.on('exit', onExit);

	kernel.send({task}, (error) => error && onError(error));

	socket.on('stop', () => kernel.send({stop: 1}, (error) => error && onError(error)));


	// console.dir(specs, {depth: null});

	// specs := json data
	// start subprocess
	// return socket id / key
};
