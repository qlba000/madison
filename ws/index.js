const SocketIO = require('socket.io');

exports.init = server => exports.server = SocketIO(server, {path: '/ws'});

// module.exports = (server) =>
// {
// 	const sockio = SocketIO(server, {path: '/ws'});

// 	sockio.on('connection', (socket) =>
// 	{
// 		console.log(`${socket.id} connected`);

// 		socket.on('disconnect', () =>
// 		{
// 			console.log(`${socket.id} disconnected`);
// 		});

// 		// fire.connect(socket);
// 	});
// };
