const http = require('http');
const Logger = require('./utils/logger');

const {listen} = require('./config').server;

const init = async () =>
{
	const logger = Logger('init');

	logger.log('initializing database connection');

	await require('./db').init();

	logger.log('initializing jobs subsystem');

	require('./jobs').init();

	logger.log('initializing server');

	const server = http.createServer(require('./express'));

	logger.log('initializing websocket subsystem');

	require('./ws').init(server);

	server.once('listening', () =>
	{
		logger.info(`server listening at ${listen.host}:${listen.port}`);
		logger.info('all systems operational');
	});

	logger.log('starting server');

	server.listen(listen.port, listen.host);
};

const logger = Logger('root');

logger.log('process ID %d', process.pid);
logger.debug('starting init');

init().then(
	ret => logger.debug('init returned %+O', ret),
	err => logger.error('init thrown %s', err.message)
);
